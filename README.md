# The Open Celtic Dictionary

**NB: This repository has moved to https://gitlab.com/prvInSpace/open-dictionary-library/**

Welcome the Open Celtic Dictionary project. The goal of the project is to be able to provide speakers of Celtic languages a dictionary API with all words, conjugations, some mutations, translations, and other useful information.

It is entirely open source and free for anyone to use.

## Community

The project got it's own Discord channel here: https://discord.gg/UzaFmfV
If you need help with anything, if you got any questions, suggestions, or if you just want to pop by to have a cup of coffee or tea, then feel free to join!

## Notes

The dictionary doesn't contain all the words currently, but if given a word-type it will try to auto-generate the conjugations and other fields.

## Available dictionaries

| Language        | Code |
|-----------------|------|
| Breton          | br   |
| Irish           | ga   |
| Scottish Gaelic | gd   |
| Welsh           | cy   |

## Usage

The API is hosted on https://api.prv.cymru:13999/api/dictionary, but can also be cloned and self hosted.
The API returns the result as UTF-8 formatted JSON.

### Parameters

| Parameter | Description                                                                                         | Default      |
|-----------|-----------------------------------------------------------------------------------------------------|--------------|
| word      | The word that you want to conjugate.                                                                | Required     |
| type      | The type of the word you want to conjugate. Current available types are: adjective, noun, and verb. |              |
| lang      | The language code of the dictionary you want to use.                                                | cy           |
| search    | A flag that sets whether to search conjugated terms as well as the lookup term.                     | False        |

#### Results

If an error is accounted with the provided parameters, the API will return a JSON object with the success set to 'false'.
The field error will contain a detailed error message with the issue encountered.

If the API succeeds (meaning that it were able to handle the request, this does not mean it will return a result) the API will
return a JSON object with the success flag set to 'true' and an array called 'results' that contains all the words it found.

* For extended information about the returned results please refer to the documentation for each language.

### Examples

* Example with the Welsh word 'gweld' (to see): https://api.prv.cymru:13999/api/dictionary?word=gweld&math=verb
* Usage of the API in the bot Digi: https://gitlab.com/prvInSpace/digi


## Building form the command line

Make sure you have Java 11 installed and the java command line set to the correct verion

On Arch-based distributions:

`sudo archlinux-java set java-11-openjdk`

On debian-based distributions:

`update-java-alternatives --list
sudo update-java-alternatives --set /path/to/java/version`

Then go in the project directory and run:

`./gradlew shadowJar`

You can now run the server by executing it (exact path of the jar file may vary):

`java -jar ./build/libs/The\ Open\ Celtic\ Dictionary-1.0-SNAPSHOT-all.jar`

The api is now available on http://localhost:13999/api/dictionary.

# Contributing

If you have found a word that is conjugated wrongly or you have any other enquiry or bug report, please feel free to either leave an issue on this page or send me an email.
I always need help with expanding the dictionary and to help me translate documentation into the different Celtic languages.

# Maintainers

* Preben Vangberg &lt;prv@aber.ac.uk&gt;

