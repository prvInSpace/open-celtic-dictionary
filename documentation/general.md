# General Documentation for the Open Celtic Dictionary API End-Points

## Common information about the results

### Common

Here are the fields that all words returned by the API have in common. 

| Field        | Description                                    | Type            | Required
|--------------|------------------------------------------------|-----------------|---
| normalForm   | Is the main lookup term and lemma of the word  | String          | Yes
| type         | The type of the word                           | String          | Yes
| confirmed    | If the word was found this is set to true, otherwise if it was generated this is set to false | Bool   | Yes
| etymology    | Etymology of the word                          | String          | No
| versions     | Other alternative versions of the word         | List of strings | No
| notes        | Other notes about the usage of the word        | String          | No
| translations | Translations for the word                      | See below       | No

#### Translations

The translation field is a list of "translation units", where each translation unit contain a list of
languages, which have a list of objects containing the translations.

A translation unit is a collection of words in different languages which mean the same.
Each language is a field with a list of translations.

An example if this is to be, which looks like the following:

```json
{
  "en": [ 
    { "value": "to be" } 
  ],
  "cy": [
    { 
      "id": 0,
      "value": "bod"
    }
  ]
}
```

As you can see, the English translation is essentially a string literal where as the Welsh version is a bit more
complicated. The reason for this that the index of the word is also added if it exist.
In the case of bod, it got the id 0.

There exist cases where one word on one language might translate to several in another language. That
is why each language is a list.

#### Mutations

For languages that have mutations, the words will also have a field called "mutations" which will
have a JSON object containing the different mutated forms. These fields will always exist, but they might be empty.
Look up the information about mutations for you specific language.

### Verbs

As well as common fields as listed above, verbs also contains a lot of tenses. These are
variables in the word object itself. This is also something that varies a lot, so please refer to the 
documentation for the specific language.

These tenses are objects, and contain the inflected versions as variables inside it. Some languages have additional inflected forms (such as Irish)
so make sure to look in the documentation for the individual language.

For verbs that have inflected tenses these usually looks like this:

```json
{
  "normalForm": "my word",
  "present": {
    "singFirst": [ "Singular First" ],
    "singSecond": [ "Singular Second" ],
    "singThird": [ "Singular Third" ],
    "plurFirst": [ "Plural First" ],
    "plurSecond": [ "Plural Second" ],
    "plurThird": [ "Plural Third" ]
  }
}
```

#### List of all tenses in all languages

Even though a tense is listed for a specific language, does not mean that it is required for every verb.
Some verbs are defective, and some have additional tenses. 

* R means that it is "Required", and it will show up unless the verb is defective. (Will be generated)
* O means that it is "Optional" which means that it exist for the language, but will only show up on some verbs. (Will not be generated)

| Tense                   | Breton | Irish | Scottish Gaelic | Welsh |
|-------------------------|--------|-------|-----------------|-------|
| present                 | R      | R     | O               | O     |
| present_independent     |        | O     |                 |       |
| present_dependent       |        | O     |                 |       |
| present_negative        |        | O     |                 |       |
| present_situative       | O      |       |                 |       |
| present_habitual        | O      | O     |                 |       |
| preterite               | R      | R     | R               | R     |
| past_independent        |        | O     |                 |       |
| past_dependent          |        | O     |                 |       |
| past_habitual           |        | O     |                 |       |
| pluperfect              |        |       |                 | R     |
| imperfect               | R      | R     |                 | O     |
| imperfect_situative     | O      |       |                 |       |
| imperfect_habitual      | O      |       |                 |       |
| future                  | R      | R     | R               | R     |
| future_independent      |        | O     |                 |       |
| future_dependent        |        | O     |                 |       |
| future_relative         |        |       | R               |       |
| conditional             |        | R     | R               | R     |
| conditional_present     | R      |       |                 |       |
| conditional_imperfect   | R      |       |                 |       |
| conditional_independent |        | O     |                 |       |
| conditional_dependent   |        | O     |                 |       |
| imperative              | R      | R     | R               | R     |

### Nouns

Nouns are most likelly never generated as most information about them are hard to generate.
The information varies from language to language, but they might have these optional fields in common:

| Field      | Description                                    | Type            | Required
|------------|------------------------------------------------|-----------------|---
| plurals    | Plural versions of the word                    | List of strings | No
| gender     | The gender of the word                         | String          | No

### Prepositions

## /api/dictionary

This is the main API endpoint, and is the end-point used to access dictionary entries.

If a type is provided, and the dictionary got no entries matching the word provided, it will
try to auto-generate the result.

### Input variables

| Parameter | Description                                                                                         | Default      |
|-----------|-----------------------------------------------------------------------------------------------------|--------------|
| word      | The word that you want to conjugate.                                                                | Required     |
| type      | The type of the word you want to conjugate. Current available types are: adjective, noun, and verb. |              |
| lang      | The language code of the dictionary you want to use.                                                | cy           |
| search    | A flag that sets whether to search conjugated terms as well as the lookup term.                     | False        |

### Result

The API will return a JSON object as long as it manages to handle the HTTP request.
The JSON object should always have a success flag (boolean) indicating if the request was handled properly.

#### Error
If the parameters provided is invalid, or any other internal error occurred, the success flag will be set
to false, and a field named error will give some indication into what went wrong.

Example:
```json
{
  "success": false,
  "error": "..."
}
```

#### Success

If the request was handled correctly, then the success flag will be set to true,
and an array with words called result.
If the API is unable to find a result, and
if the type variable is empty,
or the specified word type don't support generating, the resulting array might be empty.

What these results contain varies from language to language and from word type to word type,
but they all have some common fields. These are listed below.

These are:

So an example result will look like this:

```json
{
  "success": true,
  "results": [
    {
      "normalForm": "myword",
      "type": "verb",
      "versions": [
        "myAlternativeVersion"
      ],
      "etymology": "my etymology",
      "notes": "my notes",
      ...
    },
    ...
  ]
}
```


## /api/dictionary/generate

This API end point is for testing new dictionary entries. It requires the language, word type, and the JSON.
The JSON format is the same as the JSON used in the exception JSON files, and therefore varies from
language to language and word type to word type.

For more information about what fields a word type for a specific language has please refer to the documentation for the individual
language if it exists.

### Input variables
| Parameter | Description                                                                                         | Default  |
|-----------|-----------------------------------------------------------------------------------------------------|----------|
| json      | The word that you want to conjugate.                                                                | Required |
| type      | The type of the word you want to conjugate. Current available types are: adjective, noun, and verb. | Required |
| lang      | The language code of the dictionary you want to use.                                                | Required |


### Result

The resulting JSON has the same format as the one returned from /api/dictionary, though
it will always only return one item in the results array.

## /api/dictionary/info

## /api/dictionary/number
