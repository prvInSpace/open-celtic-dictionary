# Documentation for the Breton dictionary

## Returned results

### Adjectives

Currently not supported

### Nouns

Currently not supported

### Verbs

All tenses comes as a JSON object with the following String fields:

* singFirst
* singSecond
* singThird
* plurFirst
* plurSecond
* plurThird
* impersonal

These might be empty, but should never be null.

Supported tenses:

| Tense                 | Default  |
|-----------------------|----------|
| Present               | true     |
| Present Situative     | false    |
| Present Habitual      | false    |
| Preterite             | true     |
| Imperfect             | true     |
| Imperfect Situative   | false    |
| Imperfect Habitual    | false    |
| Future                | true     |
| Imperative            | true     |
| Conditional Present   | true     |
| Conditional Imperfect | true     |

Note: Even though the ones labled default will usually be present, some words might not have all tenses. In that case the tense will not be present in the results.

### Mutations

All words will have the auto-generated field "mutations" unless they have the flagg "mutates" set to false in the dictionary. This is a JSON object
that contains the follow String fields

* init (unmutated)
* soft
* spirant
* hard
* mixed

