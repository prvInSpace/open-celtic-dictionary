package cymru.prv.api.geiriadur.httphandlers;

import com.sun.net.httpserver.HttpExchange;
import cymru.prv.api.geiriadur.APIServer;
import cymru.prv.api.geiriadur.linguistics.common.Word;
import cymru.prv.api.geiriadur.linguistics.common.WordType;
import org.json.JSONArray;

import java.util.Map;

public class GeneralHttpHandler extends AbstractHttpHandler {

    public GeneralHttpHandler(APIServer server) {
        super(server);
    }

    @Override
    protected void handleRequest(HttpExchange exchange, Map<String, String> getVariables) {

        if(getVariables.containsKey("id")) {
            long id;
            try{
                id = Long.parseLong(getVariables.get("id"));
            }
            catch (Exception e){
                sendBadRequest(exchange, "Id is not a number");
                return;
            }
            sendValidResponse(exchange, server.getDictionaries().getWordAsJsonArrayByIndex(id));
            return;
        }
        
        // Get word
        if(!getVariables.containsKey("word")) {
            sendBadRequest(exchange, "Missing field 'word'");
            return;
        }
        String word = getVariables.get("word");

        if(word.isEmpty() && !word.trim().isEmpty()){
            sendBadRequest(exchange, "Word provided is empty or whitespace");
            return;
        }

        // Get type
        WordType type = null;
        if(getVariables.containsKey("type")){
            try{
                type = WordType.valueOf(getVariables.get("type"));
            }
            catch (Exception e){
                sendBadRequest(exchange, "Unknown type '" + getVariables.get("type") + "'");
                return;
            }
        }

        // Get type
        boolean search = false;
        if(getVariables.containsKey("search")){
            try{
                search = Boolean.parseBoolean(getVariables.get("search"));
            }
            catch (Exception e){
                sendBadRequest(exchange, "");
                return;
            }
        }

        // Get language code
        String languageCode = "cy";
        if(getVariables.containsKey("lang"))
            languageCode = getVariables.get("lang");

        if(!server.getDictionaries().hasDictionaryWithLangCode(languageCode)){
                sendBadRequest(exchange, "Unknown language code '" + languageCode + "'");
                return;
        }

        //Search for the given word
        if(search){
            sendValidResponse(exchange, server.getDictionaries().searchForWord(languageCode, word, 5));
        }
        else {
            if(type != null){
                sendValidResponse(exchange, server.getDictionaries().getWord(languageCode, word, type));
            }
            else {
                sendValidResponse(exchange, server.getDictionaries().getWord(languageCode, word));
            }
        }
    }
}
