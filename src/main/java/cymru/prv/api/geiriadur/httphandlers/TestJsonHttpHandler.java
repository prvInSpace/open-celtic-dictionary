package cymru.prv.api.geiriadur.httphandlers;

import com.sun.net.httpserver.HttpExchange;
import cymru.prv.api.geiriadur.APIServer;
import cymru.prv.api.geiriadur.linguistics.common.WordType;
import org.json.JSONObject;

import java.util.Map;

public class TestJsonHttpHandler extends AbstractHttpHandler {


    /**
     * The default constructor for the AbstractHttpHandler.
     * The APIServer object is required for the HttpHandler to access the
     * DictionaryList.
     *
     * @param server The API server which the HttpHandler is associated with
     */
    public TestJsonHttpHandler(APIServer server) {
        super(server);
    }

    @Override
    protected void handleRequest(HttpExchange exchange, Map<String, String> getVariables) {
        if(!getVariables.containsKey("json")){
            sendBadRequest(exchange, "Missing field json");
        }
        else if(!getVariables.containsKey("type")){
            sendBadRequest(exchange, "Missing field type");
        }
        else if(!getVariables.containsKey("lang")){
            sendBadRequest(exchange, "Missing field type");
        }
        else{
            JSONObject object;
            try {
                object = new JSONObject(getVariables.get("json").replaceAll("\\+", " ".replaceAll("\\ {2,}", " ")));
            }
            catch (Exception e){
                sendBadRequest(exchange, e.getMessage());
                return;
            }
            WordType type = WordType.valueOf(getVariables.get("type"));
            String lang = getVariables.get("lang");
            sendValidResponse(exchange, server.getDictionaries().generateWord(lang, object, type));
        }
    }
}
