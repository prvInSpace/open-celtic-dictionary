package cymru.prv.api.geiriadur.httphandlers;

import com.sun.net.httpserver.HttpExchange;
import cymru.prv.api.geiriadur.APIServer;
import org.json.JSONObject;

import java.util.Map;

public class WordIndexListHttpHandler extends AbstractHttpHandler{

    /**
     * The default constructor for the AbstractHttpHandler.
     * The APIServer object is required for the HttpHandler to access the
     * DictionaryList.
     *
     * @param server The API server which the HttpHandler is associated with
     */
    public WordIndexListHttpHandler(APIServer server) {
        super(server);
    }

    @Override
    protected void handleRequest(HttpExchange exchange, Map<String, String> getVariables) {
        if(!getVariables.containsKey("min")){
            sendBadRequest(exchange,"Missing field: min");
            return;
        }
        if(!getVariables.containsKey("max")){
            sendBadRequest(exchange,"Missing field: max");
            return;
        }

        long min = Long.parseLong(getVariables.get("min"));
        long max = Long.parseLong(getVariables.get("max"));
        if(max-min >= 1000)
            max = min+999;

        JSONObject results = new JSONObject();
        for(long i = min; i <= max; ++i){
            var word = server.getDictionaries().getWordAtIndex(i);
            if(word != null)
                results.put("" + i, word.getNormalForm());
        }

        JSONObject obj = new JSONObject()
                .put("success", true)
                .put("min", min)
                .put("max", max)
                .put("results", results);

        sendJSONResponse(exchange, obj);
    }
}
