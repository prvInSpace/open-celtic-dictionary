package cymru.prv.api.geiriadur.httphandlers;

import com.sun.net.httpserver.HttpExchange;
import cymru.prv.api.geiriadur.APIServer;
import cymru.prv.api.geiriadur.linguistics.breton.BretonFemNumberToText;
import cymru.prv.api.geiriadur.linguistics.breton.BretonMascNumberToText;
import cymru.prv.api.geiriadur.linguistics.common.ConvertNumberToText;
import org.json.JSONObject;

import java.util.Map;

public class NumbersHttpHandler extends AbstractHttpHandler {

    Map<String, ConvertNumberToText> numberConverters = Map.of(
            "br_masc", new BretonMascNumberToText(),
        "br_fem", new BretonFemNumberToText()
    );

    /**
     * The default constructor for the AbstractHttpHandler.
     * The APIServer object is required for the HttpHandler to access the
     * DictionaryList.
     *
     * @param server The API server which the HttpHandler is associated with
     */
    public NumbersHttpHandler(APIServer server) {
        super(server);
    }

    @Override
    protected void handleRequest(HttpExchange exchange, Map<String, String> getVariables) {
        if(!getVariables.containsKey("number")){
            sendBadRequest(exchange, "Missing field number");
            return;
        }
        try {
            long number = Long.parseLong(getVariables.get("number"));
            JSONObject obj = new JSONObject().put("success", true).put("number", number);
            for (String key : numberConverters.keySet()) {
                obj.put(key, numberConverters.get(key).convertToText(number));
            }
            sendJSONResponse(exchange, obj);
        }
        catch (Exception e){
            sendBadRequest(exchange, "Could not parse '" + getVariables.get("number") + "' to a long");
        }
    }
}
