package cymru.prv.api.geiriadur.httphandlers;

import com.sun.net.httpserver.HttpExchange;
import cymru.prv.api.geiriadur.APIServer;
import org.json.JSONObject;

import java.util.Map;

public class TranslationHttpHandler extends AbstractHttpHandler {
    /**
     * The default constructor for the AbstractHttpHandler.
     * The APIServer object is required for the HttpHandler to access the
     * DictionaryList.
     *
     * @param server The API server which the HttpHandler is associated with
     */
    public TranslationHttpHandler(APIServer server) {
        super(server);
    }

    @Override
    protected void handleRequest(HttpExchange exchange, Map<String, String> getVariables) {
        if(!getVariables.containsKey("word")){
            sendBadRequest(exchange, "Missing field word");
            return;
        }
        String word = getVariables.get("word");
        sendJSONResponse(exchange,
                new JSONObject()
                        .put("results", server.getDictionaries().getTranslationsByEnglishWord(word))
                        .put("word", word)
                        .put("success", true));
    }
}
