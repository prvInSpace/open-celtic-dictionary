package cymru.prv.api.geiriadur.httphandlers;

import com.sun.net.httpserver.HttpExchange;
import cymru.prv.api.geiriadur.APIServer;
import cymru.prv.api.geiriadur.AbstractDictionary;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Map;

public class InfoHttpHandler extends AbstractHttpHandler {
    /**
     * The default constructor for the AbstractHttpHandler.
     * The APIServer object is required for the HttpHandler to access the
     * DictionaryList.
     *
     * @param server The API server which the HttpHandler is associated with
     */
    public InfoHttpHandler(APIServer server) {
        super(server);
    }

    @Override
    protected void handleRequest(HttpExchange exchange, Map<String, String> getVariables) {
        JSONObject information = new JSONObject();
        information.put("numberOfIndexedWords", server.getDictionaries().getNumberOfWords());
        JSONArray languages = new JSONArray();
        for (AbstractDictionary dictionary : server.getDictionaries().getDictionaries()) {
            languages.put(dictionary.getInfoAsJson());
        }
        information.put("dictionaries", languages);
        sendJSONResponse(exchange, information);
    }
}
