package cymru.prv.api.geiriadur.linguistics.manx;

import cymru.prv.api.geiriadur.linguistics.common.Noun;
import org.json.JSONObject;

public class ManxNoun extends Noun {
    public ManxNoun(JSONObject obj) {
        super(obj);
    }
}
