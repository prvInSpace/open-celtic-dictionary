package cymru.prv.api.geiriadur.linguistics.common;

import org.json.JSONObject;

public interface WordReference {
    JSONObject getJson();
}
