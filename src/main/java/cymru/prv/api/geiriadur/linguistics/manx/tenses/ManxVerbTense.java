package cymru.prv.api.geiriadur.linguistics.manx.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import cymru.prv.api.geiriadur.linguistics.common.VerbTense;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class ManxVerbTense extends VerbTense {

    public ManxVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected String apply(String suffix) {
        return null;
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultSingThird() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.emptyList();
    }
}
