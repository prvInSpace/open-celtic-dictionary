package cymru.prv.api.geiriadur.linguistics.irish.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import cymru.prv.api.geiriadur.linguistics.irish.IrishLenition;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class FirstImperfectIrishVerbTense extends LenitedIrishVerbTense {

    public FirstImperfectIrishVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.singletonList(applyBroadOrSlender("ainn", "inn"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.singletonList(applyBroadOrSlender("tá", "teá"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(applyBroadOrSlender("aimis", "imis"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.singletonList(applyBroadOrSlender("aidis", "idis"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlender("taí", "tí"));
    }

    @Override
    protected List<String> defaultAnalytic() {
        return Collections.singletonList(applyBroadOrSlender("adh", "eadh"));
    }
}
