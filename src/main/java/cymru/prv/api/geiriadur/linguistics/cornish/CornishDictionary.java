package cymru.prv.api.geiriadur.linguistics.cornish;

import cymru.prv.api.geiriadur.AbstractDictionary;
import cymru.prv.api.geiriadur.DictionaryList;
import cymru.prv.api.geiriadur.linguistics.common.Conjunction;
import cymru.prv.api.geiriadur.linguistics.common.InflectedPreposition;
import cymru.prv.api.geiriadur.linguistics.common.Word;
import cymru.prv.api.geiriadur.linguistics.common.WordType;
import org.json.JSONObject;

import java.util.Map;
import java.util.function.Function;

public class CornishDictionary extends AbstractDictionary {

    private static final Map<WordType, Function<JSONObject, Word>> wordTypeFromJsonMap = Map.of(
            WordType.verb, CornishVerb::new,
            WordType.adjective, CornishAdjective::new,
            WordType.conjunction, Conjunction::new,
            WordType.noun, CornishNoun::new,
            WordType.preposition, InflectedPreposition::new
    );

    public CornishDictionary(DictionaryList dictionaryList) {
        super(dictionaryList, "Cornish", "kw", wordTypeFromJsonMap);
    }

    @Override
    protected Map<String, String> getLanguageNameTranslations() {
        return null;
    }
}
