package cymru.prv.api.geiriadur.linguistics.irish.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class SecondPresentIrishVerbTense extends IrishVerbTense {

    public SecondPresentIrishVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.singletonList(applyBroadOrSlender("aím", "ím"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultSingThird() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(applyBroadOrSlender("aímid", "ímid"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultPlurThird() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlender("aítear", "ítear"));
    }

    @Override
    protected List<String> defaultAnalytic() {
        return Collections.singletonList(applyBroadOrSlender("aíonn", "íonn"));
    }
}
