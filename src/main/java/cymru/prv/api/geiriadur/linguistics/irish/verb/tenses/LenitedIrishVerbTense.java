package cymru.prv.api.geiriadur.linguistics.irish.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import cymru.prv.api.geiriadur.linguistics.irish.IrishLenition;
import org.json.JSONObject;

public abstract class LenitedIrishVerbTense extends IrishVerbTense {

    protected final boolean hasLenition;

    public LenitedIrishVerbTense(Verb verb, JSONObject obj, boolean defaultHasLenition){
        super(verb, obj);
        hasLenition = obj.optBoolean("hasLenition", defaultHasLenition);
    }

    public LenitedIrishVerbTense(Verb verb, JSONObject obj) {
        this(verb, obj, true);
    }

    protected String applyBroadOrSlenderWithoutLenition(String broad, String slender){
        return super.applyBroadOrSlender(broad, slender);
    }

    @Override
    protected String applyBroadOrSlender(String broad, String slender) {
        if(hasLenition)
            return IrishLenition.preformLenition(super.applyBroadOrSlender(broad, slender));
        else
            return super.applyBroadOrSlender(broad, slender);
    }

}
