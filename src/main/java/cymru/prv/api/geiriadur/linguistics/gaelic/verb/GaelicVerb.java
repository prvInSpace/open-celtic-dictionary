package cymru.prv.api.geiriadur.linguistics.gaelic.verb;

import java.util.regex.Pattern;

import cymru.prv.api.geiriadur.linguistics.common.SpecialVerbTense;
import org.json.JSONObject;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import cymru.prv.api.geiriadur.linguistics.gaelic.verb.tenses.GaelicConditionalVerbTense;
import cymru.prv.api.geiriadur.linguistics.gaelic.verb.tenses.GaelicFutureVerbTense;
import cymru.prv.api.geiriadur.linguistics.gaelic.verb.tenses.GaelicImperativeVerbTense;
import cymru.prv.api.geiriadur.linguistics.gaelic.verb.tenses.GaelicPastVerbTense;
import cymru.prv.api.geiriadur.linguistics.gaelic.verb.tenses.GaelicRelativeFutureVerbTense;

public class GaelicVerb extends Verb {

    public static final String FUTURE_RELATIVE = "future_relative";

    /**
     * Reads data from the JSON object
     * and fills out the necessary fields.
     *
     * @param obj a JSON object containing at least
     *            the field "normalForm". The field
     *            "notes" is optional
     */
    public GaelicVerb(JSONObject obj) {
        super(obj);
    }


    @Override
    protected String getStem(String normalForm) {
        return normalForm;
    }

    @Override
    protected void registerTenses(JSONObject obj) {
            // no simple present exists in gaelic
            addRequiredTense(obj, PRETERITE, GaelicPastVerbTense::new);
            addRequiredTense(obj, FUTURE, GaelicFutureVerbTense::new);
            addRequiredTense(obj, CONDITIONAL, GaelicConditionalVerbTense::new);
            addRequiredTense(obj, IMPERATIVE, GaelicImperativeVerbTense::new);
            addRequiredTense(obj, FUTURE_RELATIVE, GaelicRelativeFutureVerbTense::new);

            //Special tense for bí
            addOptionalTense(obj, PRESENT, SpecialVerbTense::new);
    }

    private static final String  regexVowels = "(aío|aoi|aoú|ia(i|)|ua(i|)|eái|ea(i|)|eo(i|)|uío|uói|iái|iói|iúi|uái|oío|" +
            "ae(i|)|ái|ai|ao|aí|éa|eá|ei|éi|ío|" +
            "uó|oi|iá|ió|iú|io|uí|úi|iu|uá|ói|oí|ui|á|a|e|é|i|í|o|ó|u|ú)";

    private long countSyllables(String normalForm){
        return Pattern.compile(regexVowels).matcher(normalForm).results().count();
    }

    public static String replaceLast(String text, String regex, String replacement) {
        return text.replaceFirst("(?s)(.*)" + regex, "$1" + replacement);
    }
    
}
