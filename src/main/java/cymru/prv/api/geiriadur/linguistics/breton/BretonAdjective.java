package cymru.prv.api.geiriadur.linguistics.breton;

import cymru.prv.api.geiriadur.linguistics.common.Adjective;
import cymru.prv.api.geiriadur.json.JsonHelperFunctions;
import cymru.prv.api.geiriadur.linguistics.common.Mutates;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class BretonAdjective extends Adjective implements Mutates {

    private final String stem;

    private final List<String> exclamative;

    public BretonAdjective(JSONObject obj) {
        super(obj);
        stem = obj.optString("stem", getStem(normalForm));
        exclamative = JsonHelperFunctions.getStringListOrNull(obj, "exclamative");
    }

    protected String getStem(String normalForm) {
        // Create stem
        return normalForm.replaceFirst("(a|i|o|u|an|añ|at|ein|ed|eg|et|in|iñ|yd)$", "")
                // provecation
                .replaceFirst("b$", "p")
                .replaceFirst("z$", "s");
    }

    @Override
    protected String getEquative() {
        return "ken " + normalForm;
    }

    @Override
    protected String getComparative() {
        return stem + "oc'h";
    }

    @Override
    protected String getSuperlative() {
        return stem + "añ";
    }

    private List<String> getExclamative(){
        return exclamative != null ? exclamative : Collections.singletonList(stem + "at");
    }

    @Override
    public JSONObject getMutations() {
        return BretonMutation.getMutations(normalForm);
    }

    @Override
    public JSONObject toJson() {
        return super.toJson().put("exclamative", new JSONArray(getExclamative()));
    }

    @Override
    public long getNumberOfVersions() {
        return super.getNumberOfVersions() + (comparable ? 1 : 0);
    }
}
