package cymru.prv.api.geiriadur.linguistics.breton.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class PresentBretonVerbTense extends BretonVerbTense {

    public PresentBretonVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.singletonList(apply("an"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.singletonList(apply("ez"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Collections.singletonList(apply(""));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(apply("omp"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Collections.singletonList(apply("it"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.singletonList(apply("ont"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(apply("er"));
    }
}
