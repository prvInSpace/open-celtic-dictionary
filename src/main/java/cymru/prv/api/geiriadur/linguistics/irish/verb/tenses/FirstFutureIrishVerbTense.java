package cymru.prv.api.geiriadur.linguistics.irish.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class FirstFutureIrishVerbTense extends IrishVerbTense {

    public FirstFutureIrishVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultAnalytic() {
        return Collections.singletonList(applyBroadOrSlender("faidh", "fidh"));
    }

    @Override
    protected List<String> defaultSingFirst() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultSingSecond() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultSingThird() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(applyBroadOrSlender("faimid", "fimid"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultPlurThird() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlender("far", "fear"));
    }
}
