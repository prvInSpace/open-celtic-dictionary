package cymru.prv.api.geiriadur.linguistics.manx;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

public class ManxVerb extends Verb {
    /**
     * Reads data from the JSON object
     * and fills out the necessary fields.
     *
     * @param obj a JSON object containing at least
     *            the field "normalForm". The field
     *            "notes" is optional
     */
    public ManxVerb(JSONObject obj) {
        super(obj);
    }

    @Override
    protected String getStem(String normalForm) {
        return normalForm;
    }

    @Override
    protected void registerTenses(JSONObject obj) {

    }
}
