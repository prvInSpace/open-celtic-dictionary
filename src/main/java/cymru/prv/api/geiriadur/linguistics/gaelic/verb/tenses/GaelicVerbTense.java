package cymru.prv.api.geiriadur.linguistics.gaelic.verb.tenses;

import java.util.List;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import cymru.prv.api.geiriadur.linguistics.common.VerbTense;
import cymru.prv.api.geiriadur.linguistics.irish.IrishLenition;

/**
 * 
 * @author Zander Urq. (zsharp68@gmail.com)
 * @since 06-03-2020 (MM-DD-YYYY)
 */
public abstract class GaelicVerbTense extends VerbTense {

    private List<String> analytic;
    protected List<String> defaultAnalytic() {
		return null;
	}

    private List<String>getAnalytic(){
        return getValue(analytic, this::defaultAnalytic);
    }
    
    public GaelicVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
        analytic = getStringListOrNull(obj, "analytic");
    }

    @Override
    public JSONObject toJson() {
        return super.toJson().put("analytic", getAnalytic());
    }

    @Override
    protected String apply(String suffix) {
        if(stem.endsWith("" + suffix.charAt(0)))
            return stem + suffix.substring(1);
        return stem + suffix;
    }

    protected String applyBroadOrSlender(String broad, String slender) {
        if(IrishLenition.isBroad(stem))
            return apply(broad);
        else
            return apply(slender);
    }

}
