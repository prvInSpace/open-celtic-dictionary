package cymru.prv.api.geiriadur.linguistics.gaelic.verb.tenses;

import java.util.Collections;
import java.util.List;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

/**
 * 
 * @author Zander Urq. (zsharp68@gmail.com)
 * @since 06-03-2020 (MM-DD-YYYY)
 */
public class GaelicImperativeVerbTense extends GaelicVerbTense {

	public GaelicImperativeVerbTense(Verb verb, JSONObject obj) {
		super(verb, obj);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected List<String> defaultSingFirst() {
		return Collections.singletonList(applyBroadOrSlender("am", "eam"));
	}

	@Override
	protected List<String> defaultSingSecond() {
		return defaultImpersonal();
	}

	@Override
	protected List<String> defaultSingThird() {
		return Collections.singletonList(applyBroadOrSlender("adh", "eadh"));
	}

	@Override
	protected List<String> defaultPlurFirst() {
		return Collections.singletonList(applyBroadOrSlender("amaid", "eamaid"));
	}

	@Override
	protected List<String> defaultPlurSecond() {
		return Collections.singletonList(applyBroadOrSlender("aibh", "ibh"));
	}

	@Override
	protected List<String> defaultPlurThird() {
		return Collections.singletonList(applyBroadOrSlender("adh", "eadh"));
	}

	@Override
	protected List<String> defaultImpersonal() {
		return Collections.singletonList(stem);
	}

}
