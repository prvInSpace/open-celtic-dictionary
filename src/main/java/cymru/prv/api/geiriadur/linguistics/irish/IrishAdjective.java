package cymru.prv.api.geiriadur.linguistics.irish;

import cymru.prv.api.geiriadur.linguistics.common.Adjective;
import org.json.JSONObject;

public class IrishAdjective extends Adjective {

    public IrishAdjective(JSONObject obj) {
        super(obj);
    }

    @Override
    protected String getEquative() {
        return null;
    }

    @Override
    protected String getComparative() {
        return null;
    }

    @Override
    protected String getSuperlative() {
        return null;
    }
}
