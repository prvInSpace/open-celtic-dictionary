package cymru.prv.api.geiriadur.linguistics.cornish.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

public abstract class Subjunctive extends CornishVerbTense {

    public Subjunctive(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected String getStem(Verb verb) {
        String stem = super.getStem(verb);
        for (Tuple tuple : stemEndingChanges) {
            if (stem.endsWith(tuple.key)) {
                return stem.replaceFirst(tuple.key + "$", tuple.value);
            }
        }
        return stem;
    }

    private static class Tuple {
        String key;
        String value;

        Tuple(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    private static Tuple[] stemEndingChanges = new Tuple[]{
            new Tuple("dhl", "tthl"),
            new Tuple("dhr", "tthr"),
            new Tuple("dhw", "tthw"),
            new Tuple("ldr", "ltr"),
            new Tuple("mbl", "mpl"),
            new Tuple("mbr", "pr"),
            new Tuple("ndl", "ntl"),
            new Tuple("ndr", "ntr"),
            new Tuple("ngr", "nkr"),
            new Tuple("rdr", "rtr"),
            new Tuple("rdh", "rth"),
            new Tuple("thl", "tthl"),
            new Tuple("thr", "tthr"),
            new Tuple("bl", "ppl"),
            new Tuple("br", "ppr"),
            new Tuple("ch", "cch"),
            new Tuple("dh", "tth"),
            new Tuple("dr", "ttr"),
            new Tuple("gh", "ggh"),
            new Tuple("gl", "kkl"),
            new Tuple("gn", "kkn"),
            new Tuple("he", "hah"),
            new Tuple("kl", "kkl"),
            new Tuple("kn", "kkn"),
            new Tuple("kr", "kkr"),
            new Tuple("ld", "lt"),
            new Tuple("lv", "lf"),
            new Tuple("nd", "nt"),
            new Tuple("ng", "nk"),
            new Tuple("nj", "nch"),
            new Tuple("rd", "rt"),
            new Tuple("rg", "rk"),
            new Tuple("rj", "rch"),
            new Tuple("rv", "rf"),
            new Tuple("sh", "ssh"),
            new Tuple("sl", "ssl"),
            new Tuple("sn", "ssn"),
            new Tuple("sw", "ssw"),
            new Tuple("th", "tth"),
            new Tuple("tl", "ttl"),
            new Tuple("vn", "ffn"),
            new Tuple("vr", "ffr"),
            new Tuple("b", "pp"),
            new Tuple("d", "tt"),
            new Tuple("f", "ff"),
            new Tuple("g", "kk"),
            new Tuple("j", "cch"),
            new Tuple("k", "kk"),
            new Tuple("l", "ll"),
            new Tuple("m", "mm"),
            new Tuple("n", "nn"),
            new Tuple("s", "ss"),
            new Tuple("p", "pp"),
            new Tuple("r", "rr"),
            new Tuple("t", "tt"),
            new Tuple("v", "ff")
    };

}
