package cymru.prv.api.geiriadur.linguistics.cornish.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class Preterite extends CornishVerbTense {

    public Preterite(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected boolean useSecondForm(Verb verb) {
        return verb.getNormalForm().endsWith("el");
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.singletonList(applyWithAffectation("is"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.singletonList(applyWithAffectation("sys"));
    }

    @Override
    protected List<String> defaultSingThird() {
        if(useSecondForm)
            return Collections.singletonList(applyWithAffectation("is"));
        if(endsInYa)
            return Collections.singletonList(apply("yas"));
        return Collections.singletonList(apply("as"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(applyWithAffectation("syn"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Collections.singletonList(applyWithAffectation("sowgh"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.singletonList(apply("sons"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return defaultSingThird();
    }
}
