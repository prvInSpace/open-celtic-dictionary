package cymru.prv.api.geiriadur.linguistics.cornish.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import cymru.prv.api.geiriadur.linguistics.common.VerbTense;
import cymru.prv.api.geiriadur.linguistics.cornish.CornishVerb;
import org.json.JSONObject;

public abstract class CornishVerbTense extends VerbTense {

    protected String affectationStem;
    protected boolean useSecondForm;
    protected boolean endsInYa;

    public CornishVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
        endsInYa = verb.getNormalForm().endsWith("ya");
        useSecondForm = obj.optBoolean("useSecondForm", useSecondForm(verb));
        affectationStem = obj.optString("affectationStem", ((CornishVerb)verb).getAffectationStem());
    }

    protected boolean useSecondForm(Verb verb){
        return false;
    }

    protected String apply(String stem, String suffix){
        return (stem + suffix).replaceAll("(.)(\\1)(\\1|s)","$1$3"); //mmm mms -> $1$3
    }

    @Override
    protected String apply(String suffix) {
        return apply(stem, suffix);
    }

    protected String applyWithAffectation(String suffix){
        if(affectationStem != null)
            return apply(affectationStem, suffix);
        return apply(suffix);
    }

}
