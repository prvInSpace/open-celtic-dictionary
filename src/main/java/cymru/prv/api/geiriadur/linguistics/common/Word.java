package cymru.prv.api.geiriadur.linguistics.common;

import cymru.prv.api.geiriadur.DictionaryList;
import cymru.prv.api.geiriadur.json.JsonHelperFunctions;
import cymru.prv.api.geiriadur.json.JsonSerializable;
import cymru.prv.api.geiriadur.linguistics.welsh.Mutation;
import cymru.prv.api.geiriadur.translations.TranslationUnit;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Word implements JsonSerializable {

    public static long tempId = -1;

    private final long id;
    private final WordType type;
    protected boolean onlyOnType;
    protected boolean mutates;
    protected String normalForm;
    protected String notes;
    protected String etymology;

    private final Map<String, String> IPAs = new HashMap<>();

    private final List<Long> versionsIndexes = new LinkedList<>();
    private final List<WordReference> versions = new LinkedList<>();

    private final List<Long> relatedIndexes = new LinkedList<>();
    private final List<WordReference> related = new LinkedList<>();

    private final List<Long> synonymsIndexes = new LinkedList<>();
    private final List<WordReference> synonyms = new LinkedList<>();

    private final List<Long> antonymIndexes = new LinkedList<>();
    private final List<WordReference> antonyms = new LinkedList<>();

    private final List<Long> troponymsIndexes = new LinkedList<>();
    private final List<WordReference> troponyms = new LinkedList<>();

    private final List<UsageExample> examples;

    protected List<TranslationUnit> translations = new LinkedList<>();

    /**
     * Reads data from the JSON object
     * and fills out the necessary fields.
     *
     * @param obj a JSON object containing at least
     *            the field "normalForm". The field
     *            "notes" is optional
     */
    public Word(JSONObject obj, WordType type){
        id = obj.optLong("id", tempId--);
        normalForm = obj.getString("normalForm");
        if(normalForm.trim().isBlank())
            throw new IllegalArgumentException("Normal form cannot be empty.\n" + obj.toString());
        notes = obj.optString("notes");
        etymology = obj.optString("etymology");
        mutates = obj.optBoolean("mutates", true);
        onlyOnType = obj.optBoolean("onlyOnType", false);

        if(obj.has("ipa")){
            JSONArray array = obj.getJSONArray("ipa");
            for(int i = 0; i < array.length(); ++i){
                JSONObject ipa = array.getJSONObject(i);
                IPAs.put(ipa.getString("key"), ipa.getString("value"));
            }
        }

        examples = JsonHelperFunctions.jsonObjectArrayToListOrNull(obj, "examples", UsageExample::new);

        splitIdValueArray(obj.optJSONArray("versions"), versions, versionsIndexes);
        splitIdValueArray(obj.optJSONArray("related"), related, relatedIndexes);
        splitIdValueArray(obj.optJSONArray("synonyms"), synonyms, synonymsIndexes);
        splitIdValueArray(obj.optJSONArray("antonyms"), antonyms, antonymIndexes);
        splitIdValueArray(obj.optJSONArray("troponyms"), troponyms, troponymsIndexes);

        this.type = type;
    }

    public void splitIdValueArray(JSONArray array, List<WordReference> references, List<Long> indexes){
        if(array == null)
            return;
        for(int i = 0; i < array.length(); ++i){
            Object obj = array.get(i);
            if(obj instanceof String)
                references.add(new LiteralWordReference((String) obj));
            else
                indexes.add(array.getLong(i));
        }
    }

    public void postLinker(DictionaryList list){
        versions.addAll(list.indexListToWordList(versionsIndexes));
        related.addAll(list.indexListToWordList(relatedIndexes));
        synonyms.addAll(list.indexListToWordList(synonymsIndexes));
        antonyms.addAll(list.indexListToWordList(antonymIndexes));
        troponyms.addAll(list.indexListToWordList(troponymsIndexes));
    }


    /**
     * Converts the word into a JSON object containing all the
     * conjugations, mutations, and other information that the
     * word may contain.
     *
     * @return the word as a JSONObject.
     */
    public JSONObject toJson(){
        JSONObject obj = new JSONObject();
        obj.put("normalForm", normalForm);
        if(this instanceof Mutates)
            if(mutates)
                obj.put("mutations", ((Mutates)this).getMutations());
        obj.put("type", type);
        if(notes != null && !notes.trim().isEmpty())
            obj.put("notes", notes);
        if(etymology != null && !etymology.trim().isEmpty())
            obj.put("etymology", etymology);

        if(id >= 0)
            obj.put("id", id);

        obj.put("translations", JsonHelperFunctions.serializableObjectArrayToJsonArray(translations));

        JSONArray ipas = new JSONArray();
        for(String key : IPAs.keySet())
            ipas.put(new JSONObject().put("key", key).put("value", IPAs.get(key)));

        if(examples != null)
            obj.put("examples", JsonHelperFunctions.serializableObjectArrayToJsonArray(examples));

        if(versions.size() > 0)
            obj.put("versions", JsonHelperFunctions.wordReferenceListToIdValueArray(versions));
        if(related.size() > 0)
            obj.put("related", JsonHelperFunctions.wordReferenceListToIdValueArray(related));
        if(synonyms.size() > 0)
            obj.put("synonyms", JsonHelperFunctions.wordReferenceListToIdValueArray(synonyms));
        if(antonyms.size() > 0)
            obj.put("antonyms", JsonHelperFunctions.wordReferenceListToIdValueArray(antonyms));
        if(troponyms.size() > 0)
            obj.put("troponyms", JsonHelperFunctions.wordReferenceListToIdValueArray(troponyms));

        return obj;
    }

    public String getNormalForm() {
        return normalForm;
    }

    public List<String> getVersions(){
        return versions.stream().filter(x -> x instanceof LiteralWordReference).map(x -> ((LiteralWordReference) x).getValue()).collect(Collectors.toList());
    }

    public boolean isOnlyOnType() {
        return onlyOnType;
    }

    public long getNumberOfVersions(){
        int numberOfMutations = 0;
        if(this instanceof Mutates) {
            JSONObject mutations = Mutation.getMutations(normalForm);
            if (mutates)
                numberOfMutations = (mutations.get(Mutation.SOFT).equals("") ? 0 : 1)
                        + (mutations.get(Mutation.NASAL).equals("") ? 0 : 1)
                        + (mutations.get(Mutation.ASPIRATE).equals("") ? 0 : 1);
        }
        return 1 + numberOfMutations + versions.size();
    }

    public boolean matchesSearch(String search){
        return normalForm.equals(search)
                || versions.stream().filter(x -> x instanceof LiteralWordReference).anyMatch(s -> ((LiteralWordReference) s).getValue().matches(search))
                || Mutation.getMutationsAsList(normalForm).stream().anyMatch(s -> s.matches(search));
    }

    public long getId() {
        return id;
    }

    public void addTranslation(TranslationUnit translation){
        translations.add(translation);
    }
}
