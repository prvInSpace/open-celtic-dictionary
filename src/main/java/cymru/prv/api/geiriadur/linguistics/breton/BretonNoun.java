package cymru.prv.api.geiriadur.linguistics.breton;

import cymru.prv.api.geiriadur.linguistics.common.Mutates;
import cymru.prv.api.geiriadur.linguistics.common.Noun;
import org.json.JSONObject;

public class BretonNoun extends Noun implements Mutates {

    public BretonNoun(JSONObject obj) {
        super(obj);
    }

    @Override
    public JSONObject getMutations() {
        return BretonMutation.getMutations(normalForm);
    }
}
