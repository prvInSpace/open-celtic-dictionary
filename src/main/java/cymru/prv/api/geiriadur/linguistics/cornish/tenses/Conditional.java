package cymru.prv.api.geiriadur.linguistics.cornish.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Conditional extends CornishVerbTense {

    public Conditional(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.singletonList(apply("sen"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.singletonList(apply("ses"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Collections.singletonList(apply("sa"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(apply("sen"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Collections.singletonList(apply("sewgh"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.singletonList(apply("sens"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(apply("sys"));
    }
}
