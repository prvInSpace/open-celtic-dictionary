package cymru.prv.api.geiriadur.linguistics.irish.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class SecondFutureIrishVerbTense extends IrishVerbTense {

    public SecondFutureIrishVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultAnalytic() {
        return Collections.singletonList(applyBroadOrSlender("óidh", "eoidh"));
    }

    @Override
    protected List<String> defaultSingFirst() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultSingSecond() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultSingThird() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(applyBroadOrSlender("óimid", "eoimid"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultPlurThird() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlender("ófar","eofar"));
    }
}
