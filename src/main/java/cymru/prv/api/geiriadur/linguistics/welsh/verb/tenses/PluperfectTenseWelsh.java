package cymru.prv.api.geiriadur.linguistics.welsh.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class PluperfectTenseWelsh extends WelshVerbTense {

    public PluperfectTenseWelsh(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Arrays.asList(apply("aswn"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Arrays.asList(apply("asit"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Arrays.asList(apply("asai"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Arrays.asList(apply("asem"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Arrays.asList(apply("asech"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Arrays.asList(apply("asent"));
    }
}
