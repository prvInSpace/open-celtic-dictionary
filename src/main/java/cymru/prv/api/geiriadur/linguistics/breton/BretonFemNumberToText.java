package cymru.prv.api.geiriadur.linguistics.breton;

public class BretonFemNumberToText extends BretonMascNumberToText {

    @Override
    protected String getSubTwenty(long number) {
        number %= 20;
        if(number == 2)
            return "div";
        if(number == 3)
            return "teir";
        if(number == 4)
            return "peder";
        return super.getSubTwenty(number);
    }
}
