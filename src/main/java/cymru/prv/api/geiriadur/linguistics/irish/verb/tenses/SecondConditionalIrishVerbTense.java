package cymru.prv.api.geiriadur.linguistics.irish.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class SecondConditionalIrishVerbTense extends LenitedIrishVerbTense {

    public SecondConditionalIrishVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultAnalytic() {
        return Collections.singletonList(applyBroadOrSlender("ódh", "oedh"));
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.singletonList(applyBroadOrSlender("óinn", "eionn"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.singletonList(applyBroadOrSlender("ófa", "eófa"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(applyBroadOrSlender("óimis", "eoimis"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.singletonList(applyBroadOrSlender("óidís", "eoidís"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlender("ófaí", "eofaí"));
    }
}
