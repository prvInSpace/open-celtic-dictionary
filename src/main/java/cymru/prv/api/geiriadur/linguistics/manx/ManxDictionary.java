package cymru.prv.api.geiriadur.linguistics.manx;

import cymru.prv.api.geiriadur.AbstractDictionary;
import cymru.prv.api.geiriadur.DictionaryList;
import cymru.prv.api.geiriadur.linguistics.common.Conjunction;
import cymru.prv.api.geiriadur.linguistics.common.InflectedPreposition;
import cymru.prv.api.geiriadur.linguistics.common.Word;
import cymru.prv.api.geiriadur.linguistics.common.WordType;
import org.json.JSONObject;

import java.util.Map;
import java.util.function.Function;

public class ManxDictionary extends AbstractDictionary {

    private static final Map<WordType, Function<JSONObject, Word>> wordTypeFromJsonMap = Map.of(
            WordType.verb, ManxVerb::new,
            WordType.adjective, ManxAdjective::new,
            WordType.noun, ManxNoun::new,
            WordType.preposition, InflectedPreposition::new,
            WordType.conjunction, Conjunction::new
    );

    public ManxDictionary(DictionaryList dictionaryList) {
        super(dictionaryList, "Manx", "gv", wordTypeFromJsonMap);
    }

    @Override
    protected Map<String, String> getLanguageNameTranslations() {
        return Map.of(
                "cy", "Manaweg"
        );
    }
}
