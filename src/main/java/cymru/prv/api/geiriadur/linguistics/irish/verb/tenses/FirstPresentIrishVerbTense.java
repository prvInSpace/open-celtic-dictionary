package cymru.prv.api.geiriadur.linguistics.irish.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class FirstPresentIrishVerbTense extends IrishVerbTense {


    public FirstPresentIrishVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.singletonList(applyBroadOrSlender("aim", "im"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultSingThird() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(applyBroadOrSlender("aimid", "imid"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultPlurThird() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlender("tar", "tear"));
    }

    @Override
    protected List<String> defaultAnalytic() {
        return Collections.singletonList(applyBroadOrSlender("ann", "eann"));
    }
}
