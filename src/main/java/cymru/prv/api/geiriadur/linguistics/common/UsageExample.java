package cymru.prv.api.geiriadur.linguistics.common;

import cymru.prv.api.geiriadur.json.JsonSerializable;
import org.json.JSONObject;

public class UsageExample implements JsonSerializable {

    private String exampleTargetLang;
    private String exampleTranslation;
    private String explanation;

    public UsageExample(JSONObject obj){
        exampleTargetLang = obj.getString("text");
        exampleTranslation = obj.optString("trans", "");
        explanation = obj.optString("explanation", "");
    }

    public JSONObject toJson(){
        return new JSONObject()
                .put("text", exampleTargetLang)
                .put("trans", exampleTranslation)
                .put("explanation", explanation);
    }

}
