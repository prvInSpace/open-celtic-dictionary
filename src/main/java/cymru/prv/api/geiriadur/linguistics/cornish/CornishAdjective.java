package cymru.prv.api.geiriadur.linguistics.cornish;

import cymru.prv.api.geiriadur.linguistics.common.Adjective;
import org.json.JSONObject;

public class CornishAdjective extends Adjective {

    public CornishAdjective(JSONObject obj) {
        super(obj);
    }

    @Override
    protected String getEquative() {
        return null;
    }

    @Override
    protected String getComparative() {
        return null;
    }

    @Override
    protected String getSuperlative() {
        return null;
    }
}
