package cymru.prv.api.geiriadur.linguistics.irish;

import cymru.prv.api.geiriadur.AbstractDictionary;
import cymru.prv.api.geiriadur.DictionaryList;
import cymru.prv.api.geiriadur.linguistics.common.*;
import cymru.prv.api.geiriadur.linguistics.irish.verb.IrishVerb;
import org.json.JSONObject;

import java.util.Map;
import java.util.function.Function;

public class IrishDictionary extends AbstractDictionary {

    private static final String languageName = "Irish";
    private static final String languageCode = "ga";
    private static final Map<WordType, Function<JSONObject, Word>> wordTypeFromJsonMap = Map.of(
            WordType.verb, IrishVerb::new,
            WordType.noun, IrishNoun::new,
            WordType.preposition, InflectedPreposition::new,
            WordType.adjective, IrishAdjective::new,
            WordType.conjunction, Conjunction::new
    );

    public IrishDictionary(){
        this(null);
    }

    public IrishDictionary(DictionaryList dictionaryList) {

        super(dictionaryList, languageName, languageCode, wordTypeFromJsonMap);
    }

    @Override
    protected Map<String, String> getLanguageNameTranslations() {
        return Map.of(
                "cy", "Gwyddeleg"
        );
    }
}
