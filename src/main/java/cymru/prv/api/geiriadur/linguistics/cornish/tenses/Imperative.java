package cymru.prv.api.geiriadur.linguistics.cornish.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class Imperative extends CornishVerbTense{

    public Imperative(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultSingSecond() {
        if(endsInYa)
            return List.of(apply(""), apply("y"));
        return Collections.singletonList(apply(""));
    }

    @Override
    protected List<String> defaultSingThird() {
        if(endsInYa)
            return Collections.singletonList(apply("yes"));
        return Collections.singletonList(apply("es"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(apply("yn"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        if(endsInYa)
            return Collections.singletonList(apply("yewgh"));
        return Collections.singletonList(apply("ewgh"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        if(endsInYa)
            return Collections.singletonList(apply("yens"));
        return Collections.singletonList(apply("ens"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.emptyList();
    }
}
