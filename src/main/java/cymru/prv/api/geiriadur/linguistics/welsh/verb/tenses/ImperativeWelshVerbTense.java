package cymru.prv.api.geiriadur.linguistics.welsh.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ImperativeWelshVerbTense extends WelshVerbTense {

    public ImperativeWelshVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Arrays.asList(apply("a"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Arrays.asList(apply("wch"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.emptyList();
    }

    @Override
    public long numberOfConjugations() {
        return 2;
    }
}
