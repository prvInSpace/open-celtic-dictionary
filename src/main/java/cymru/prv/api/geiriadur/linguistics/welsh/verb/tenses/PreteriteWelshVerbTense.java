package cymru.prv.api.geiriadur.linguistics.welsh.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PreteriteWelshVerbTense extends WelshVerbTense {

    public PreteriteWelshVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.singletonList(apply("ais"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.singletonList(apply("aist"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Collections.singletonList(apply("odd"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(apply("on"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Collections.singletonList(apply("och"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.singletonList(apply("on"));
    }

}
