package cymru.prv.api.geiriadur.linguistics.cornish;

import cymru.prv.api.geiriadur.httphandlers.AbstractHttpHandler;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Mutation contains several static functions relating to
 * mutations of words in the Welsh language.
 *
 * An instance of the class represent a single row in
 * the mutation table.
 *
 * @author Preben Vangberg
 * @since 2020-04-21
 */
public class Mutation {

    public static final String SOFT = "soft";
    public static final String HARD = "hard";
    public static final String ASPIRATE = "aspirate";
    public static final String MIXED = "mixed";

    private String init;
    private String soft;
    private String hard;
    private String aspirate;
    private String mixed;

    private Mutation(String init, String soft, String aspirate, String hard, String mixed) {
        this.init = init;
        this.soft = soft;
        this.aspirate = aspirate;
        this.hard = hard;
        this.mixed = mixed;
    }

    private static final Map<String, Mutation> mutationMap = createMutationTable();

    private static final Mutation defaultMutation = new Mutation(null, null, null, null, null);

    private static Map<String, Mutation> createMutationTable(){
        return Map.of(
                "p", new Mutation("p", "b" , "f", null, null),
                "t", new Mutation("t", "d" , "th", null, null),
                "k", new Mutation("k", "g" , "h", null, null),
                "b", new Mutation("b", "v", null, "p", "f"),
                "d", new Mutation("d", "dh", null, "t", "t"),
                "gr?(i|y|e|a)", new Mutation("g", "-", null, "k", "h"),
                "gr?(o|u)", new Mutation("g", "w", null, "k", "hw"),
                "gw", new Mutation("gw", "w", null, "kw", "hw"),
                "m", new Mutation("m", "v", null, null, "f"),
                "ch", new Mutation("rh", "r", null, null, null));
    }

    /**
     * Creates a new mutation table and returns it.
     *
     * @return a map of mutations.
     */
    public static Map<String, Mutation> getMutationTable(){
        return createMutationTable();
    }

    /**
     * Creates a JSONObject containing the different mutations
     * of the given word. These are theoretical and may not
     * appear in the real word.
     *
     * @param word The word to be mutated.
     * @return the mutations of the word as a JSONObject.
     */
    public static JSONObject getMutations(String word){
        word = word.toLowerCase();
        JSONObject obj = new JSONObject();
        Mutation mutation = getMutation(word);
        obj.put("init", word);
        obj.put(SOFT, mutation.getSoft(word));
        obj.put(ASPIRATE, mutation.getAspirate(word));
        obj.put(HARD, mutation.getHard(word));
        obj.put(MIXED, mutation.getMixed(word));
        return obj;
    }

    public static List<String> getMutationsAsList(String word){
        word = word.toLowerCase();
        Mutation mutation = getMutation(word);
        List<String> muts = new LinkedList<>();
        muts.add(mutation.getSoft(word));
        muts.add(mutation.getAspirate(word));
        muts.add(mutation.getHard(word));
        muts.add(mutation.getMixed(word));
        return muts;
    }

    private static Mutation getMutation(String word){
        for(String regex : mutationMap.keySet())
            if(word.matches("^" + regex + ".*"))
                return mutationMap.get(regex);
        return defaultMutation;
    }

    private String mutateWord(String word, String init, String replacement){
        if(replacement == null)
            return "";
        if(replacement.equals("-"))
            return word.substring(1);
        return word.replaceFirst(init, replacement);
    }

    public String getSoft(String word) {
        return mutateWord(word, init, soft);
    }

    public String getHard(String word) {
        return mutateWord(word, init, hard);
    }

    public String getAspirate(String word) {
        return mutateWord(word, init, aspirate);
    }

    public String getMixed(String word) {
        return mutateWord(word, init, mixed);
    }
}
