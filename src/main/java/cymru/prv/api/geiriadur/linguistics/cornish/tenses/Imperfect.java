package cymru.prv.api.geiriadur.linguistics.cornish.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class Imperfect extends CornishVerbTense {

    public Imperfect(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected boolean useSecondForm(Verb verb) {
        return verb.getNormalForm().matches(".*(el|es|he|i)$");
    }

    @Override
    protected List<String> defaultSingFirst() {
        if(useSecondForm)
            return Collections.singletonList(apply("yn"));
        if(endsInYa)
            return Collections.singletonList(apply("yen"));
        return Collections.singletonList(apply("en"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        if(useSecondForm)
            return Collections.singletonList(apply("ys"));
        if(endsInYa)
            return Collections.singletonList(apply("yes"));
        return Collections.singletonList(apply("es"));
    }

    @Override
    protected List<String> defaultSingThird() {
        if(useSecondForm)
            return Collections.singletonList(apply("i"));
        if(endsInYa)
            return Collections.singletonList(apply("ya"));
        return Collections.singletonList(apply("a"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return defaultSingFirst();
    }

    @Override
    protected List<String> defaultPlurSecond() {
        if(endsInYa)
            return Collections.singletonList(apply("yewgh"));
        return Collections.singletonList(apply("ewgh"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        if(endsInYa)
            return Collections.singletonList(apply("yens"));
        return Collections.singletonList(apply("ens"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(apply("ys"));
    }
}
