package cymru.prv.api.geiriadur.linguistics.breton.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class ImperfectBretonVerbTense extends BretonVerbTense {

    public ImperfectBretonVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.singletonList(apply("en"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.singletonList(apply("es"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Collections.singletonList(apply("e"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(apply("emp"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Collections.singletonList(apply("ec'h"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.singletonList(apply("ent"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(apply("ed"));
    }
}
