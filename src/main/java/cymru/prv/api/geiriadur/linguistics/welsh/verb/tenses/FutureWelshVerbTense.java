package cymru.prv.api.geiriadur.linguistics.welsh.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class FutureWelshVerbTense extends WelshVerbTense {

    public FutureWelshVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Arrays.asList(apply("af"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Arrays.asList(apply("i"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Arrays.asList(
                apply("iff"),
                apply("ith")
        );
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Arrays.asList(apply("wn"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Arrays.asList(apply("wch"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Arrays.asList(apply("an"));
    }
}
