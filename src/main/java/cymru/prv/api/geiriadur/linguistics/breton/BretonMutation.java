package cymru.prv.api.geiriadur.linguistics.breton;

import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Mutation contains several static functions relating to
 * mutations of words in the Welsh language.
 *
 * An instance of the class represent a single row in
 * the mutation table.
 *
 * @author Preben Vangberg
 * @since 2020-04-21
 */
public class BretonMutation {

    private String init;
    private String soft;
    private String spirant;
    private String hard;
    private String mixed;

    private BretonMutation(String init, String soft, String spirant, String hard, String mixed) {
        this.init = init;
        this.soft = soft;
        this.spirant = spirant;
        this.hard = hard;
        this.mixed = mixed;
    }

    private static final Map<String, BretonMutation> mutationMap = createMutationTable();

    private static final BretonMutation defaultMutation = new BretonMutation(null, null, null, null, null);

    private static Map<String, BretonMutation> createMutationTable(){
        return Map.of(
                "p", new BretonMutation("p", "b" , "f", null, null),
                "t", new BretonMutation("t", "d" , "z", null, null),
                "k", new BretonMutation("k", "g" , "c'h", null, null),
                "b", new BretonMutation("b", "v", null, "p", "v"),
                "d", new BretonMutation("d", "z", null, "t", "t"),
                "g", new BretonMutation("g", "c'h", null, "k", "c'h"),
                "gw", new BretonMutation("gw", "w", null, "kw", "w"),
                "m", new BretonMutation("m", "v", null, null, "v"));
    }

    /**
     * Creates a new mutation table and returns it.
     *
     * @return a map of mutations.
     */
    public static Map<String, BretonMutation> getMutationTable(){
        return createMutationTable();
    }

    /**
     * Creates a JSONObject containing the different mutations
     * of the given word. These are theoretical and may not
     * appear in the real word.
     *
     * @param word The word to be mutated.
     * @return the mutations of the word as a JSONObject.
     */
    public static JSONObject getMutations(String word){
        word = word.toLowerCase();
        JSONObject obj = new JSONObject();
        BretonMutation mutation = getMutation(word);
        obj.put("init", word);
        obj.put("soft", mutation.getMutationString(word, mutation.soft));
        obj.put("spirant", mutation.getMutationString(word, mutation.spirant));
        obj.put("hard", mutation.getMutationString(word, mutation.hard));
        obj.put("mixed", mutation.getMutationString(word, mutation.mixed));
        return obj;
    }

    private static BretonMutation getMutation(String word){
        if(word.startsWith("gw"))
            return mutationMap.get("gw");
         return mutationMap.getOrDefault(word.substring(0,1), defaultMutation);
    }

    private String getMutationString(String word, String replacement){
        if(replacement == null)
            return "";
        if(replacement.equals("-"))
            return word.substring(1);
        return word.replaceFirst("^" + init, replacement);
    }

    public static String getSpirant(String word){
        BretonMutation mutation = getMutation(word);
        String mutated = mutation.getMutationString(word, mutation.spirant);
        if(mutated != null)
            return mutated;
        return  word;
    }

    public static String getSoft(String word){
        BretonMutation mutation = getMutation(word);
        String mutated = mutation.getMutationString(word, mutation.soft);
        if(mutated != null)
            return mutated;
        return  word;
    }
}
