package cymru.prv.api.geiriadur.linguistics.common;

import org.json.JSONObject;

public class IDWordReference implements WordReference {

    public final Word word;

    public IDWordReference(Word word){
        this.word = word;
    }

    @Override
    public JSONObject getJson() {
        return new JSONObject().put("id", word.getId()).put("value", word.getNormalForm());
    }
}
