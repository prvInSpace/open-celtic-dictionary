package cymru.prv.api.geiriadur.linguistics.common;

public enum WordType {
    verb, adjective, noun, preposition, conjunction
}
