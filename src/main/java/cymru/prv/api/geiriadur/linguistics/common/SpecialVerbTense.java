package cymru.prv.api.geiriadur.linguistics.common;

import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class SpecialVerbTense extends VerbTense {

    public SpecialVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected String apply(String suffix) {
        return null;
    }

    @Override
    protected List<String> defaultSingFirst() {
        return getDefaultOrFunction(Collections::emptyList);
    }

    @Override
    protected List<String> defaultSingSecond() {
        return getDefaultOrFunction(Collections::emptyList);
    }

    @Override
    protected List<String> defaultSingThird() {
        return getDefaultOrFunction(Collections::emptyList);
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return getDefaultOrFunction(Collections::emptyList);
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return getDefaultOrFunction(Collections::emptyList);
    }

    @Override
    protected List<String> defaultPlurThird() {
        return getDefaultOrFunction(Collections::emptyList);
    }

    @Override
    protected List<String> defaultImpersonal() {
        return getDefaultOrFunction(Collections::emptyList);
    }
}
