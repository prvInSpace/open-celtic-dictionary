package cymru.prv.api.geiriadur.linguistics.common;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

public abstract class VerbTense {

    protected abstract String apply(String suffix);

    protected String stem;
    private boolean hasTense;
    private List<String> singFirst;
    private List<String> singSecond;
    private List<String> singThird;
    private List<String> plurFirst;
    private List<String> plurSecond;
    private List<String> plurThird;
    private List<String> impersonal;
    protected List<String> defaults;

    protected abstract List<String> defaultSingFirst();
    protected abstract List<String> defaultSingSecond();
    protected abstract List<String> defaultSingThird();
    protected abstract List<String> defaultPlurFirst();
    protected abstract List<String> defaultPlurSecond();
    protected abstract List<String> defaultPlurThird();
    protected abstract List<String> defaultImpersonal();

    public VerbTense(Verb verb, JSONObject obj){
        this.stem = getStem(verb);
        parseJSON(obj);
    }

    protected String getStem(Verb verb){
        return verb.stem;
    }

    protected List<String> getValue(List<String> value, Supplier<List<String>> supplier){
        if(value != null)
            return value;
        return supplier.get();
    }

    protected List<String> getDefaultOrFunction(Supplier<List<String>> otherwise){
        if(defaults == null)
            return otherwise.get();
        return defaults;
    }

    public List<String> getSingFirst() {
        return getValue(singFirst, this::defaultSingFirst);
    }

    public List<String> getSingSecond() {
        return getValue(singSecond, this::defaultSingSecond);
    }

    public List<String> getSingThird() {
        return getValue(singThird, this::defaultSingThird);
    }

    public List<String> getPlurFirst() {
        return getValue(plurFirst, this::defaultPlurFirst);
    }

    public List<String> getPlurSecond() {
        return getValue(plurSecond, this::defaultPlurSecond);
    }

    public List<String> getPlurThird() {
        return getValue(plurThird, this::defaultPlurThird);
    }

    public List<String> getImpersonal(){
        return getValue(impersonal, this::defaultImpersonal);
    }

    public boolean hasTense() {
        return hasTense;
    }

    public JSONObject toJson(){
        JSONObject obj = new JSONObject();
        obj.put("singFirst", getSingFirst());
        obj.put("singSecond", getSingSecond());
        obj.put("singThird", getSingThird());
        obj.put("plurFirst", getPlurFirst());
        obj.put("plurSecond", getPlurSecond());
        obj.put("plurThird", getPlurThird());
        obj.put("impersonal", getImpersonal());
        return obj;
    }

    private void parseJSON(JSONObject object){
        stem = object.optString("stem", stem);
        hasTense = object.optBoolean("has", true);
        if(!hasTense)
            return;
        singFirst = getStringListOrNull(object, "singFirst");
        singSecond = getStringListOrNull(object, "singSecond");
        singThird = getStringListOrNull(object, "singThird");
        plurFirst = getStringListOrNull(object, "plurFirst");
        plurSecond = getStringListOrNull(object, "plurSecond");
        plurThird = getStringListOrNull(object, "plurThird");
        impersonal = getStringListOrNull(object, "impersonal");
        defaults = getStringListOrNull(object, "defaults");
    }

    public long numberOfConjugations(){
        if(stem == null)
            return 0;
        if(!hasTense)
            return 0;
        return getSingFirst().size() +
                getSingSecond().size() +
                getSingThird().size() +
                getPlurFirst().size() +
                getPlurSecond().size() +
                getPlurThird().size() +
                getImpersonal().size();
    }

    public boolean hasMatch(String word){
        if(!hasTense)
            return false;
        if(getSingFirst().stream().anyMatch(x -> x.matches(word)))
            return true;
        if(getSingSecond().stream().anyMatch(x -> x.matches(word)))
            return true;
        if(getSingThird().stream().anyMatch(x -> x.matches(word)))
            return true;
        if(getPlurFirst().stream().anyMatch(x -> x.matches(word)))
            return true;
        if(getPlurSecond().stream().anyMatch(x -> x.matches(word)))
            return true;
        if(getPlurThird().stream().anyMatch(x -> x.matches(word)))
            return true;
        return false;
    }
    
    protected List<String> getStringListOrNull(JSONObject obj, String key){
        if(obj.has(key)) {
            List<String> list = new LinkedList<>();
            Object conjugation = obj.get(key);
            if(conjugation instanceof JSONArray)
                for(int i = 0; i < ((JSONArray) conjugation).length(); ++i)
                    list.add(((JSONArray) conjugation).getString(i));
            else
                list.add((String)conjugation);
            return list;
        }
        return null;
    }
}
