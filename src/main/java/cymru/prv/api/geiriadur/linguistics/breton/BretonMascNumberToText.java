package cymru.prv.api.geiriadur.linguistics.breton;

import cymru.prv.api.geiriadur.linguistics.common.ConvertNumberToText;

import java.util.function.Function;

public class BretonMascNumberToText implements ConvertNumberToText {

    @Override
    public String convertToText(long number) {
        if(number == 0)
            return "mann";
        return getSubTrilion(number).trim();
    }

    private String getSubENumber(long number, long scientific, String name, Function<Long, String> lower){
        return getSubENumber(number, scientific, name, "ur", lower);
    }

    private String getSubENumber(long number, long scientific, String name, String one, Function<Long, String> lower){
        number %= scientific;
        long numberOfUnits = number/(scientific/1000);
        if(number < scientific/1000)
            return lower.apply(number);
        if(number < 2*scientific/1000)
            return "ur " + name + lower.apply(number);
        if(number/(scientific/1000) == 2)
            return "daou " + BretonMutation.getSoft(name) + " " + lower.apply(number);
        if(numberOfUnits >= 120 && numberOfUnits % 10 == 0){
            // first second name
            return getMascSubThousand(number/(scientific/1000)) + " " +
                    getTennerWithoutPrefix(numberOfUnits) + " " + name + " " + lower.apply(number);
        }
        if(numberOfUnits/10 == 13 || numberOfUnits/10 == 15){
            // first third name second
            return (getMascSubThousand(numberOfUnits) +
                    " " + getSubTwentyPastThousand(one,numberOfUnits % 10) + " " + name + " " + getTenner(numberOfUnits)
                    + " " + lower.apply(number))
                    .replaceAll("\\s{2,}"," ");
        }
        else {
            // first third name second
            return (getMascSubThousand(numberOfUnits) +
                    " " + getSubTwentyPastThousand(one,numberOfUnits % 20) + " " + name + " " + getTenner(numberOfUnits)
                    + " " + lower.apply(number))
                    .replaceAll("\\s{2,}"," ");
        }
    }

    private String getSubQuadrillion(long number){
        return getSubENumber(number, /*1_000_000_000_000_000_000_000L*/(long)1e24, "triliard", "un", this::getSubTriliard);
    }

    private String getSubTriliard(long number){
        return getSubENumber(number, (long)1e21, "trilion", "un", this::getSubTrilion);
    }

    private String getSubTrilion(long number){
        return getSubENumber(number, (long)1e18, "biliard", this::getSubBiliard);
    }

    private String getSubBiliard(long number){
        return getSubENumber(number, (long)1e15, "bilion", this::getSubBillion);
    }

    private String getSubBillion(long number){
        return getSubENumber(number, (long)1e12, "miliard", this::getSubMilliard);
    }

    private String getSubMilliard(long number){
        return getSubENumber(number, (long)1e9, "milion", this::getSubMillion);
    }

    private String getSubMillion(long number){
        return getSubENumber(number, (long)1e6, "mil", "unan", this::getSubThousand);
    }

    private String getMascSubThousand(long number){
        number %= 1000;
        long hundred = number/100;
        if(hundred == 1)
            return "kant";
        if(hundred == 2 || hundred == 3|| hundred == 4 || hundred == 9)
            return getSubTwentyPastThousand("", hundred) + " c'hant";
        else
            return getSubTwentyPastThousand("", hundred) + " kant";
    }

    private String getTennerWithoutPrefix(long number){
        number %= 100;
        if(number < 20)
            return "";
        if(number < 30)
            return "ugent";
        if(number < 40)
            return "tregont";
        if(number < 50)
            return "daou-ugent";
        if(number < 60)
            return "hanter-kant";
        if(number < 80)
            return "tri-ugent";
        else
            return "pevar-ugent";
    }

    private String getTenner(long number){
        number %= 100;
        if(number < 20)
            return "";
        if(number < 30)
            return "warn-ugent";
        if(number < 40)
            return "ha tregont";
        if(number < 50)
            return "ha daou-ugent";
        if(number < 60)
            return "hag hanter-kant";
        if(number < 80)
            return "ha tri-ugent";
        else
            return "ha pevar-ugent";
    }

    private String getSubTwentyPastThousand(String one, long number){
        if(number == 1)
            return one;
        else
            return getMaskSubTwenty(number);
    }

    /**
     * Foundamentals WORKS kinda.
     */
    private String getSubThousand(long number){
        number %= 1000;
        long hundred = number / 100;
        if(number < 100)
            return getSubHundred(number);
        if(number == 100)
            return "kant";
        if(hundred < 2)
            return "kant" + (number % 100 != 0 ? " " + getSubHundred(number) : "");
        if(hundred == 2 || hundred == 3|| hundred == 4 || hundred == 9)
            return getMaskSubTwenty(hundred) + " c'hant" + (number % 100 != 0 ? " " + getSubHundred(number) : "");
        else
            return getMaskSubTwenty(hundred) + " kant" + (number % 100 != 0 ? " " + getSubHundred(number) : "");
    }

    private String getSubHundred(long number){
        number %= 100;
        if(number < 20)
            return getSubTwenty(number);
        if(number == 20)
            return "ugent";
        if(number < 30)
            return getSubTwenty(number) + " warn-ugent";
        if(number == 30)
            return "tregont";
        if(number < 40)
            return getSubTwenty(number % 10) + " ha tregont";
        if(number == 40)
            return "daou-ugent";
        if(number < 50)
            return getSubTwenty(number) + " ha daou-ugent";
        if(number == 50)
            return "hanter-kant";
        if(number < 60)
            return getSubTwenty(number % 10) + " hag hanter-kant";
        if(number == 60)
            return "tri-ugent";
        if(number < 80)
            return getSubTwenty(number) + " ha tri-ugent";
        if(number == 80)
            return "pevar-ugent";
        else
            return getSubTwenty(number) + " ha pevar-ugent";
    }

    private String getMaskSubTwenty(long number){
        number %= 20;
        if(number == 0)
            return "";
        else if(number == 1)
            return "unan";
        else if(number == 2)
            return "daou";
        else if(number == 3)
            return "tri";
        else if(number == 4)
            return "pevar";
        else if(number == 5)
            return "pemp";
        else if(number == 6)
            return "c'hwec'h";
        else if(number == 7)
            return "seizh";
        else if(number == 8)
            return "eizh";
        else if(number == 9)
            return "nav";
        else if(number == 10)
            return "dek";
        else if(number == 11)
            return "unnek";
        else if(number == 12)
            return "daouzek";
        else if(number == 13)
            return "trizek";
        else if(number == 14)
            return "pevarzek";
        else if(number == 15)
            return "pemzek";
        else if(number == 16)
            return "c'hwezek";
        else if(number == 17)
            return "seitek";
        else if(number == 18)
            return "triwec'h";
        else
            return "naontek";
    }

    protected String getSubTwenty(long number){
        return this.getMaskSubTwenty(number);
    }
}
