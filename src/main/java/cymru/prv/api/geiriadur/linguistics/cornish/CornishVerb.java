package cymru.prv.api.geiriadur.linguistics.cornish;

import cymru.prv.api.geiriadur.linguistics.common.Mutates;
import cymru.prv.api.geiriadur.linguistics.common.SpecialVerbTense;
import cymru.prv.api.geiriadur.linguistics.cornish.tenses.*;
import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

public class CornishVerb extends Verb implements Mutates {

    private String affectationStem;

    /**
     * Reads data from the JSON object
     * and fills out the necessary fields.
     *
     * @param obj a JSON object containing at least
     *            the field "normalForm". The field
     *            "notes" is optional
     */
    public CornishVerb(JSONObject obj) {
        super(obj);
    }

    @Override
    protected String getStem(String normalForm) {
        return normalForm.replaceFirst("(es|ya|he|a|e|o|i|u)$", "");
    }


    @Override
    protected void registerTenses(JSONObject obj) {
        if(obj.optBoolean("hasAffectation", false))
            affectationStem = obj.optString("affectationStem", replaceLast(stem, "[ao]", "e"));

        addRequiredTense(obj, "present_future", PresentFuture::new);
        addRequiredTense(obj, IMPERFECT, Imperfect::new);
        addRequiredTense(obj, PRETERITE, Preterite::new);
        addRequiredTense(obj, CONDITIONAL, Conditional::new);
        addRequiredTense(obj, "subjunctive_present_future", SubjunctivePresentFuture::new);
        addRequiredTense(obj, "subjunctive_imperfect", SubjunctiveImperfect::new);
        addRequiredTense(obj, IMPERATIVE, Imperative::new);

        addOptionalTense(obj, PRESENT, SpecialVerbTense::new);
        addOptionalTense(obj, "present_short", SpecialVerbTense::new);
        addOptionalTense(obj, "present_long", SpecialVerbTense::new);
        addOptionalTense(obj, "imperfect_short", SpecialVerbTense::new);
        addOptionalTense(obj, "imperfect_long", SpecialVerbTense::new);
        addOptionalTense(obj, FUTURE, PresentFuture::new);
        addOptionalTense(obj, "perfect", SpecialVerbTense::new);
    }

    @Override
    public JSONObject getMutations() {
        return Mutation.getMutations(normalForm);
    }

    private static String replaceLast(String text, String regex, String replace) {
        MatchResult result = null;
        for (Iterator<MatchResult> it = Pattern.compile(regex).matcher(text).results().iterator(); it.hasNext(); ) {
            result = it.next();
        }
        if(result == null)
            return text;

        String base = text.substring(0, result.start());
        String end = text.substring(result.end());
        return base + replace + end;
    }

    public String getAffectationStem(){
        return affectationStem;
    }

}
