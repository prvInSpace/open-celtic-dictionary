package cymru.prv.api.geiriadur.linguistics.common;

import cymru.prv.api.geiriadur.json.JsonHelperFunctions;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public abstract class Verb extends Word {

    public static final String PRESENT = "present";
    public static final String PRETERITE = "preterite";
    public static final String IMPERATIVE = "imperative";
    public static final String IMPERFECT = "imperfect";
    public static final String FUTURE = "future";
    public static final String CONDITIONAL = "conditional";

    protected String stem;
    protected String verbPrefix;
    protected final boolean conjugates;
    protected List<String> verbalNoun;

    protected abstract String getStem(String normalForm);
    protected abstract void registerTenses(JSONObject obj);

    private Map<String, VerbTense> tenses = new HashMap<>();
    public Map<String, VerbTense> getTenses() {
        return tenses;
    }

    /**
     * Reads data from the JSON object
     * and fills out the necessary fields.
     *
     * @param obj  a JSON object containing at least
     *             the field "normalForm". The field
     *             "notes" is optional
     */
    public Verb(JSONObject obj) {
        super(obj, WordType.verb);
        conjugates = obj.optBoolean("conjugates", true);
        verbalNoun = JsonHelperFunctions.getStringListOrNull(obj, "verbalNoun");
        stem = obj.optString("stem", getStem(normalForm));
        if(conjugates)
            registerTenses(obj);
    }

    @Override
    public JSONObject toJson() {
        JSONObject obj = super.toJson();
        if(!conjugates)
            return obj;

        obj.put("stem", stem);
        if(verbalNoun != null)
            obj.put("verbalNoun", verbalNoun);

        for (String tenseStr : tenses.keySet()){
            JSONObject tense = tenses.get(tenseStr).toJson();
            if(verbPrefix != null) {
                for (String key : tense.keySet()) {
                    JSONArray words = tense.getJSONArray(key);
                    JSONArray newWords = new JSONArray();
                    for (int i = 0; i < words.length(); i++) {
                        newWords.put(verbPrefix + words.getString(i));
                    }
                    tense.put(key, newWords);
                }
            }
            obj.put(tenseStr, tense);
        }

        return obj;
    }

    protected void addTense(String name, VerbTense tense){
        tenses.put(name, tense);
    }

    protected void addOptionalTense(JSONObject obj, String tense,  BiFunction<Verb, JSONObject, VerbTense> constructor){
        if(obj.has(tense)){
            VerbTense tenseObj = constructor.apply(this, obj.getJSONObject(tense));
            if(tenseObj.hasTense())
                tenses.put(tense, tenseObj);
        }
    }

    protected void addRequiredTense(JSONObject obj, String tense, BiFunction<Verb, JSONObject, VerbTense> constructor){
        if(obj.has(tense)){
            VerbTense tenseObj = constructor.apply(this, obj.getJSONObject(tense));
            if(tenseObj.hasTense())
                tenses.put(tense, tenseObj);
        }
        else {
            tenses.put(tense, constructor.apply(this, new JSONObject("{}")));
        }
    }


    @Override
    public long getNumberOfVersions() {
        long number = 0;
        for(VerbTense tense : tenses.values())
            number += tense.numberOfConjugations();
        return super.getNumberOfVersions() + number;
    }

    @Override
    public boolean matchesSearch(String search) {
        if(super.matchesSearch(search))
            return true;

        for(VerbTense tense : tenses.values())
            if(tense.hasMatch(search))
                return true;

        return false;
    }

}
