package cymru.prv.api.geiriadur.linguistics.cornish.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class SubjunctivePresentFuture extends Subjunctive {

    public SubjunctivePresentFuture(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.singletonList(apply("iv"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.singletonList(apply("i"));
    }

    @Override
    protected List<String> defaultSingThird() {
        if(endsInYa)
            return Collections.singletonList(apply("yo"));
        return Collections.singletonList(apply("o"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(apply("yn"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        if(endsInYa)
            return Collections.singletonList(apply("yowgh"));
        return Collections.singletonList(apply("owgh"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        if(endsInYa)
            return Collections.singletonList(apply("yons"));
        return Collections.singletonList(apply("ons"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        if(endsInYa)
            return Collections.singletonList(apply("yer"));
        return Collections.singletonList(apply("er"));
    }
}
