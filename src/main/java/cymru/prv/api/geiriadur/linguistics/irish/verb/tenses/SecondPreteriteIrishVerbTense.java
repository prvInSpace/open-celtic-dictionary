package cymru.prv.api.geiriadur.linguistics.irish.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import cymru.prv.api.geiriadur.linguistics.irish.IrishLenition;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class SecondPreteriteIrishVerbTense extends LenitedIrishVerbTense {

    private final String normalForm;

    public SecondPreteriteIrishVerbTense(Verb verb, JSONObject obj) {
        super(verb, obj);
        this.normalForm = verb.getNormalForm();
    }

    @Override
    protected List<String> defaultAnalytic() {
        return Collections.singletonList(IrishLenition.preformLenition(normalForm));
    }

    @Override
    protected List<String> defaultSingFirst() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultSingSecond() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultSingThird() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(applyBroadOrSlender("aíomar","íomar"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultPlurThird() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlenderWithoutLenition("aíodh", "íodh"));
    }
}
