package cymru.prv.api.geiriadur.linguistics.common;

import org.json.JSONObject;

public class Conjunction extends Word {
    /**
     * Reads data from the JSON object
     * and fills out the necessary fields.
     *
     * @param obj  a JSON object containing at least
     *             the field "normalForm". The field
     *             "notes" is optional
     */
    public Conjunction(JSONObject obj) {
        super(obj, WordType.conjunction);
    }
}
