package cymru.prv.api.geiriadur.linguistics.common;

import org.json.JSONObject;

public class LiteralWordReference implements WordReference {

    private final String word;

    public LiteralWordReference(String word){
        this.word = word;
    }

    @Override
    public JSONObject getJson() {
        return new JSONObject().put("value", word);
    }

    public String getValue() {
        return word;
    }
}
