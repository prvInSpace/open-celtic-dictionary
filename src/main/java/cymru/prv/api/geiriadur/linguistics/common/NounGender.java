package cymru.prv.api.geiriadur.linguistics.common;

public enum NounGender {
    m, f, u, mf
}
