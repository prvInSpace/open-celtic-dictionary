package cymru.prv.api.geiriadur.linguistics.cornish;

import cymru.prv.api.geiriadur.linguistics.common.Mutates;
import cymru.prv.api.geiriadur.linguistics.common.Noun;
import org.json.JSONObject;

public class CornishNoun extends Noun implements Mutates {
    public CornishNoun(JSONObject obj) {
        super(obj);
    }

    @Override
    public JSONObject getMutations() {
        return Mutation.getMutations(normalForm);
    }
}
