package cymru.prv.api.geiriadur.linguistics.common;

public interface ConvertNumberToText {
    String convertToText(long number);
}
