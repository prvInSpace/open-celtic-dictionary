package cymru.prv.api.geiriadur.linguistics.cornish.tenses;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class PresentFuture extends CornishVerbTense {

    public PresentFuture(Verb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        if(endsInYa)
            return Collections.singletonList(apply("yav"));
        return Collections.singletonList(apply("av"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.singletonList(applyWithAffectation("ydh"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Collections.singletonList(apply(""));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(applyWithAffectation("yn"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        if(endsInYa)
            return Collections.singletonList(applyWithAffectation("yowgh"));
        return Collections.singletonList(applyWithAffectation("owgh"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        if(endsInYa)
            return Collections.singletonList(apply("yons"));
        return Collections.singletonList(apply("ons"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(apply("ir"));
    }
}
