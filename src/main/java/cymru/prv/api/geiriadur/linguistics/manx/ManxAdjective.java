package cymru.prv.api.geiriadur.linguistics.manx;

import cymru.prv.api.geiriadur.linguistics.common.Adjective;
import org.json.JSONObject;

public class ManxAdjective extends Adjective {

    public ManxAdjective(JSONObject obj) {
        super(obj);
    }

    @Override
    protected String getEquative() {
        return null;
    }

    @Override
    protected String getComparative() {
        return null;
    }

    @Override
    protected String getSuperlative() {
        return null;
    }
}
