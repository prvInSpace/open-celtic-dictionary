package cymru.prv.api.geiriadur.linguistics.gaelic.verb.tenses;

import java.util.Collections;
import java.util.List;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import org.json.JSONObject;

/**
 * 
 * @author Zander Urq. (zsharp68@gmail.com)
 * @since 06-03-2020 (MM-DD-YYYY)
 */
public class GaelicConditionalVerbTense extends GaelicVerbTense {

	public GaelicConditionalVerbTense(Verb verb, JSONObject obj) {
		super(verb, obj);
	}

	@Override
	protected List<String> defaultSingFirst() {
		return Collections.singletonList(applyBroadOrSlender("ainn", "inn"));
	}

	@Override
	protected List<String> defaultSingSecond() {
		return Collections.singletonList(applyBroadOrSlender("adh tu", "eadh tu"));
	}

	@Override
	protected List<String> defaultSingThird() {
		return Collections.singletonList(applyBroadOrSlender("adh e/i", "eadh e/i"));
	}

	@Override
	protected List<String> defaultPlurFirst() {
		return Collections.singletonList(applyBroadOrSlender("amaid", "eamaid"));
	}

	@Override
	protected List<String> defaultPlurSecond() {
		return Collections.singletonList(applyBroadOrSlender("adh sibh", "eadh sibh"));
	}

	@Override
	protected List<String> defaultPlurThird() {
		return Collections.singletonList(applyBroadOrSlender("adh iad", "eadh iad"));
	}

	@Override
	protected List<String> defaultImpersonal() {
		return Collections.emptyList();
	}

}
