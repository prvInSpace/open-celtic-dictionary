package cymru.prv.api.geiriadur.linguistics.common;

import cymru.prv.api.geiriadur.json.JsonHelperFunctions;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public abstract class Adjective extends Word {

    public static final String EQUATIVE = "equative";
    public static final String COMPATATIVE = "comparative";
    public static final String SUPERLATIVE = "superlative";

    protected List<String> equative;
    protected List<String> comparative;
    protected List<String> superlative;

    protected boolean comparable;

    public Adjective(JSONObject obj){
        super(obj, WordType.adjective);
        comparable =  obj.optBoolean("comparable", true);
        comparative = JsonHelperFunctions.getStringListOrNull(obj, COMPATATIVE);
        superlative = JsonHelperFunctions.getStringListOrNull(obj,SUPERLATIVE);
        equative = JsonHelperFunctions.getStringListOrNull(obj, EQUATIVE);
    }

    @Override
    public JSONObject toJson() {
        JSONObject obj = super.toJson();

        if(comparable) {
            if(comparative != null)
                obj.put(COMPATATIVE, new JSONArray(comparative));
            else
                obj.put(COMPATATIVE, new JSONArray(Collections.singletonList(getComparative())));

            if(superlative != null)
                obj.put(SUPERLATIVE, new JSONArray(superlative));
            else
                obj.put(SUPERLATIVE, new JSONArray(Collections.singletonList(getSuperlative())));

            if(equative != null)
                obj.put(EQUATIVE, new JSONArray(equative));
            else
                obj.put(EQUATIVE, new JSONArray(Collections.singletonList(getEquative())));
        }

        return obj;
    }

    protected abstract String getEquative();
    protected abstract String getComparative();
    protected abstract String getSuperlative();

    @Override
    public long getNumberOfVersions() {
        return super.getNumberOfVersions() + 3;
    }

    @Override
    public boolean matchesSearch(String search) {
        return super.matchesSearch(search)
                || (comparative != null && comparative.stream().anyMatch(x -> x.matches(search)))
                || (superlative != null && superlative.stream().anyMatch(x -> x.matches(search)))
                || (equative != null && equative.stream().anyMatch(x -> x.matches(search)))
                || getComparative().matches(search)
                || getSuperlative().matches(search)
                || getEquative().matches(search);
    }
}
