package cymru.prv.api.geiriadur.translations;

import cymru.prv.api.geiriadur.DictionaryList;
import cymru.prv.api.geiriadur.json.JsonSerializable;
import cymru.prv.api.geiriadur.linguistics.common.IDWordReference;
import cymru.prv.api.geiriadur.linguistics.common.LiteralWordReference;
import cymru.prv.api.geiriadur.linguistics.common.Word;
import cymru.prv.api.geiriadur.linguistics.common.WordReference;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TranslationUnit implements JsonSerializable {

    private Map<String, List<WordReference>> translationMap = new HashMap<>();
    private boolean ignoreDuplicate = false;

    public TranslationUnit(DictionaryList list, JSONObject obj){
        for(String languageCode : obj.keySet()){

            if(languageCode.equals("ignoreDuplicate")){
                ignoreDuplicate = obj.getBoolean("ignoreDuplicate");
                continue;
            }

            List<WordReference> translationsList = new LinkedList<>();
            translationMap.put(languageCode, translationsList);

            JSONArray translations = obj.getJSONArray(languageCode);
            for (int i = 0; i < translations.length(); i++) {
                Object translation = translations.get(i);
                if(translation instanceof String){
                    translationsList.add(new LiteralWordReference((String) translation));
                    if(languageCode.equals("en"))
                        list.addTranslationUnit((String)translation, this);
                }
                else {
                    long index = translations.getLong(i);
                    Word word = list.getWordAtIndex(index);
                    if(word != null) {
                        translationsList.add(new IDWordReference(word));
                        word.addTranslation(this);
                    }
                }
            }
        }
    }

    public JSONObject toJson(){
        JSONObject obj = new JSONObject();
        for(String langCode : translationMap.keySet()){
            JSONArray array = new JSONArray();
            for(WordReference trans : translationMap.get(langCode))
                array.put(trans.getJson());
            obj.put(langCode, array);
        }
        return obj;
    }

    public boolean ignoreDuplicate() {
        return ignoreDuplicate;
    }
}
