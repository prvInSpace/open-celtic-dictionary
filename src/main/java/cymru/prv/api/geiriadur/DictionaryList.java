package cymru.prv.api.geiriadur;

import cymru.prv.api.geiriadur.linguistics.breton.BretonDictionary;
import cymru.prv.api.geiriadur.linguistics.common.IDWordReference;
import cymru.prv.api.geiriadur.linguistics.common.Word;
import cymru.prv.api.geiriadur.linguistics.common.WordReference;
import cymru.prv.api.geiriadur.linguistics.common.WordType;
import cymru.prv.api.geiriadur.linguistics.cornish.CornishDictionary;
import cymru.prv.api.geiriadur.linguistics.gaelic.GaelicDictionary;
import cymru.prv.api.geiriadur.linguistics.irish.IrishDictionary;
import cymru.prv.api.geiriadur.linguistics.manx.ManxDictionary;
import cymru.prv.api.geiriadur.linguistics.welsh.WelshDictionary;
import cymru.prv.api.geiriadur.translations.TranslationUnit;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;


/**
 * Represents a list of dictionaries which are identified by a language code.
 * This is works as a bridge between the dictionaries, which means that it is responsible for
 * loading inter-dictionary translations.a
 *
 * @author Preben Vangberg
 * @since 2020.05.31
 */
public class DictionaryList {

    private class WordDictionaryPair {
        String dictionaryCode;
        Word word;
        protected WordDictionaryPair(AbstractDictionary dictionary, Word word){
            this.dictionaryCode = dictionary.getLanguageCode();
            this.word = word;
        }
    }

    /**
     * Indicates whether to print warnings and info
     * to the terminal.
     *
     * if true the program will print error messages,
     * otherwise it will not.
     */
    private final boolean debugMode;

    /**
     * A map with Words and the ID's of these Word objects.
     * @see Word
     */
    private Map<Long, WordDictionaryPair> wordIndexMap = new HashMap<>();

    /**
     * List of dictionaries. The key is the language code of the given dictionary
     * which is fetched from the Dictionary object itself.
     *
     * @see AbstractDictionary
     */
    private Map<String, AbstractDictionary> dictionaries = new HashMap<>();

    private Map<String, List<TranslationUnit>> englishDictionary = new HashMap<>();

    /**
     * A list of all the dictionary constructors.
     * DictionaryList will initiate all of these upon construction.
     */
    private List<Function<DictionaryList, AbstractDictionary>> dictionaryConstructors = Arrays.asList(
            WelshDictionary::new,
            BretonDictionary::new,
            IrishDictionary::new,
            GaelicDictionary::new,
            ManxDictionary::new,
            CornishDictionary::new
    );

    /**
     * Default constructor for Dictionarylist.
     *
     * This will construct all the dictionaries is dictionaryConstructors.
     * Afterwards it will load all the translations in res/trans.json and
     * add these to the required Word objects.
     *
     * @throws Exception If any error occur during construction of the object.
     */
    public DictionaryList() throws Exception {
      this(false);
    }

    public DictionaryList(boolean debug) throws IOException {
        debugMode = debug;

        // Create dictionaries
        for (Function<DictionaryList, AbstractDictionary> constructor : dictionaryConstructors){
            AbstractDictionary dictionary = constructor.apply(this);
            dictionaries.put(dictionary.getLanguageCode(), dictionary);
        }

        // Do post creation linking
        for(WordDictionaryPair wordPair : wordIndexMap.values())
            wordPair.word.postLinker(this);

        // Get translations
        File translationFolder = new File("res/trans/");
        for(File translationsFile : translationFolder.listFiles()){
            if(translationsFile.isFile()){
                JSONArray array = new JSONArray(Files.readString(translationsFile.toPath()));
                for (int i = 0; i < array.length(); i++)
                    new TranslationUnit(this, array.getJSONObject(i));
            }
        }
    }

    /**
     * Returns the word on a given index or null
     * @param index the word index you want.
     * @return Returns the word if the index exist, otherwise null.
     */
    public Word getWordAtIndex(long index) {
        WordDictionaryPair pair = wordIndexMap.getOrDefault(index, null);
        if(pair != null)
            return pair.word;
        return null;
    }

    public void addTranslationUnit(String englishWord, TranslationUnit unit){
        if(!englishDictionary.containsKey(englishWord))
            englishDictionary.put(englishWord, new LinkedList<>());
        else if(!unit.ignoreDuplicate() && debugMode)
            System.out.println("Warning: Duplicate translation entry for word '" + englishWord + "'");

        var list = englishDictionary.get(englishWord);
        if(!list.contains(unit))
            list.add(unit);

        englishWord = englishWord.replaceAll("\\(.*\\)", "").trim();

        if(!englishDictionary.containsKey(englishWord))
            englishDictionary.put(englishWord, new LinkedList<>());

        list = englishDictionary.get(englishWord);
        if(!list.contains(unit))
            list.add(unit);
    }


    /**
     * Searched for the English word in the dictionary and returns a JSON array with all the translation units.
     *
     * In case it doesn't find the word it also tries some different ways.
     *
     * 1) Add "to" to the beginning. I.e "think" -> "to think"
     * 2) Add "to", and remove "ing" from the end. I.e "thinking" -> "to think"
     *
     * @param englishWord The English word you want to search for.
     * @return A JSONArray with the results.
     */
    public JSONArray getTranslationsByEnglishWord(String englishWord){
        JSONArray array = new JSONArray();
        List<TranslationUnit> units = englishDictionary.get(englishWord);

        if(units == null)
            units = englishDictionary.get("to " + englishWord);
        if(units == null)
            units = englishDictionary.get("to " + englishWord.replaceFirst("ing$", ""));

        if(units != null)
            for(TranslationUnit unit : units)
                array.put(unit.toJson());

        return array;
    }


    /**
     * Puts the given word in the ID / Word map.
     * The ID is fetched from the word object.
     *
     * @throws IllegalArgumentException If a duplicate word entry id already exist.
     * @param word The word you want to add register the ID of.
     */
    public void putWord(AbstractDictionary dictionary, Word word) {
        if(wordIndexMap.containsKey(word.getId())) {
            throw new IllegalArgumentException("Duplicate word entry id " +
                    word.getId() + ". " + word.getNormalForm() + ", " +
                    wordIndexMap.get(word.getId()).word.getNormalForm());
        }
        wordIndexMap.put(word.getId(), new WordDictionaryPair(dictionary, word));
    }


    /**
     * @param index The index of the word you want.
     * @return A JSONArray containing the word with the given ID if it exist.
     */
    public JSONArray getWordAsJsonArrayByIndex(long index) {
        WordDictionaryPair pair = wordIndexMap.getOrDefault(index, null);
        JSONArray array = new JSONArray();
        if(pair != null)
            array.put(pair.word.toJson().put("confirmed", true).put("lang", pair.dictionaryCode));
        return array;
    }


    /**
     * Returns a the dictionary associated with a given language code.
     * @param langCode The language code of the dictionary
     * @return The dictionary if it exist. Otherwise null.
     */
    public AbstractDictionary getDictionaryByLangCode(String langCode){
        return dictionaries.get(langCode);
    }


    /**
     * Checks if a given dictionary is present in the dictionary list by its language code.
     *
     * @param langCode The language of the dictionary
     * @return Returns true if the given dictionary is present in the dictionary list
     */
    public boolean hasDictionaryWithLangCode(String langCode){
        return dictionaries.containsKey(langCode);
    }


    /**
     * Fetches the dictionary with the given language-code and gets the word from the
     * dictionary with the specified type.
     *
     * @see AbstractDictionary
     * @param languageCode The language code of the dictionary
     * @param word The word in the dictionary you want
     * @param type The type of the word you want
     * @return A JSONArray with the words or generated result.
     */
    public JSONArray getWord(String languageCode, String word, WordType type){
        return dictionaries.get(languageCode).getWord(word, type);
    }


    /**
     * Fetches the dictionary with the given language-code and gets the word from the
     * dictionary independently of the type.
     *
     * @see AbstractDictionary
     * @param languageCode the language code of the dictionary
     * @param word the word in the dictionary you want
     * @return All the words in the dictionary that matches the word
     */
    public JSONArray getWord(String languageCode, String word){
        return dictionaries.get(languageCode).getWord(word);
    }


    /**
     * Fetches the dictionary with the given language-code, and searches for match.
     *
     * @see AbstractDictionary for more information
     * @param languageCode the language code of the dictionary
     * @param word the search term
     * @param max max number of results
     * @return a JSONArray with the results from the dictionary
     */
    public JSONArray searchForWord(String languageCode, String word, int max){
        return dictionaries.get(languageCode).searchForWord(word, max);
    }


    /**
     * Get the number of words that is registered with an index.
     * @return the number of words in wordIndexMap.
     */
    public long getNumberOfWords(){
        return wordIndexMap.size();
    }


    /**
     * Returns an unmodifiable collections of the dictionaries.
     *
     * @return Returns an unmodifiable collections of the dictionaries.
     */
    public Collection<AbstractDictionary> getDictionaries(){
        return Collections.unmodifiableCollection(dictionaries.values());
    }


    /**
     * @param lang The language code of the dictionary you want
     * @param object A JSON object that will be used for the generation
     * @param type The word type of the word you want to generate
     * @return Returns a JSONArray containing the generated word.
     */
    public JSONArray generateWord(String lang, JSONObject object, WordType type){
        return dictionaries.get(lang).generateWord(object, type);
    }


    /**
     * Converts a list of indexes to a list of words.
     *
     * @param indexes List of indexes
     * @return List of words with the given indexes.
     */
    public List<WordReference> indexListToWordList(List<Long> indexes){
        List<WordReference> retVal = null;
        if(indexes != null){
            retVal = new LinkedList<>();
            for(long index : indexes) {
                Word word = getWordAtIndex(index);
                if(word != null)
                    retVal.add(new IDWordReference(word));
                else
                    throw new IllegalStateException("Dictionary list does not contains related word!");
            }
        }
        return retVal;
    }

    public boolean isDebugMode() {
        return debugMode;
    }
}
