package cymru.prv.api.geiriadur.json;

import org.json.JSONObject;

public interface JsonSerializable {
    JSONObject toJson();
}
