package cymru.prv.api.geiriadur.json;

import cymru.prv.api.geiriadur.linguistics.common.WordReference;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

public class JsonHelperFunctions {

    public static List<Long> getLongListOrNull(JSONObject obj, String key){
        List<Long> retVal = null;
        if(obj.has(key)){
            retVal = new LinkedList<>();
            Object value = obj.get(key);
            if(value instanceof JSONArray)
                for(int i = 0; i < ((JSONArray)value).length(); ++i)
                    retVal.add(((JSONArray)value).getLong(i));
            else
                retVal.add(obj.getLong(key));
        }
        return retVal;
    }

    public static List<String> getStringListOrNull(JSONObject obj, String key){
        if(obj.has(key)) {
            List<String> list = new LinkedList<>();
            Object conjugation = obj.get(key);
            if(conjugation instanceof JSONArray)
                for(int i = 0; i < ((JSONArray) conjugation).length(); ++i)
                    list.add(((JSONArray) conjugation).getString(i));
            else
                list.add((String)conjugation);
            return list;
        }
        return null;
    }

    public static List<String> getStringListOrEmptyList(JSONObject obj, String key){
        var list = getStringListOrNull(obj, key);
        if(list == null)
            return new LinkedList<>();
        return list;
    }

    public static JSONArray wordReferenceListToIdValueArray(List<WordReference> words){
        JSONArray array = new JSONArray();
        if(words != null)
            for(WordReference word : words)
                array.put(word.getJson());
        return array;
    }

    public static <T extends JsonSerializable> JSONArray serializableObjectArrayToJsonArray(List<T>  objects){
        JSONArray array = new JSONArray();
        for (JsonSerializable obj : objects)
            array.put(obj.toJson());
        return array;
    }

    public static <T> List<T> jsonObjectArrayToListOrNull(JSONObject obj, String key, Function<JSONObject, T> converter){
        if(!obj.has(key))
            return null;

        List<T> list = new LinkedList<>();
        JSONArray array = obj.getJSONArray(key);
        for(int i = 0; i < array.length(); ++i)
            list.add(converter.apply(array.getJSONObject(i)));

        return list;
    }
}
