package cymru.prv.api.geiriadur.irish;

import cymru.prv.api.geiriadur.linguistics.irish.IrishLenition;
import org.junit.Assert;
import org.junit.Test;

public class IrishLenitionTests {

    @Test
    public void testSh(){
        Assert.assertEquals("sraon", IrishLenition.preformLenition("sraon"));
        Assert.assertEquals("sholáthair", IrishLenition.preformLenition("soláthair"));
    }

}
