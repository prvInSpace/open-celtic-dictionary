package cymru.prv.api.geiriadur.irish;

import cymru.prv.api.geiriadur.common.AbstractTestClass;
import cymru.prv.api.geiriadur.linguistics.irish.IrishDictionary;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class IrishVerbTests extends AbstractTestClass {

    public static void testTense(
            JSONObject tense,
            String[] singFirst,
            String[] singSecond,
            String[] singThird,
            String[] plurFirst,
            String[] plurSecond,
            String[] plurThird,
            String[] impersonal,
            String[] analytic
    ){

        Assert.assertArrayEquals(singFirst, jsonArrayToStringArray(tense.getJSONArray("singFirst")));
        Assert.assertArrayEquals(singSecond, jsonArrayToStringArray(tense.getJSONArray("singSecond")));
        Assert.assertArrayEquals(singThird, jsonArrayToStringArray(tense.getJSONArray("singThird")));

        Assert.assertArrayEquals(plurFirst, jsonArrayToStringArray(tense.getJSONArray("plurFirst")));
        Assert.assertArrayEquals(plurSecond, jsonArrayToStringArray(tense.getJSONArray("plurSecond")));
        Assert.assertArrayEquals(plurThird, jsonArrayToStringArray(tense.getJSONArray("plurThird")));

        Assert.assertArrayEquals(impersonal, jsonArrayToStringArray(tense.getJSONArray("impersonal")));
        Assert.assertArrayEquals(analytic, jsonArrayToStringArray(tense.getJSONArray("analytic")));
    }

    @Test
    public void testDéan(){
        JSONObject obj = getVerb(new IrishDictionary(), "déan");

        testTense(
                obj.getJSONObject("present"),
                "déanaim", "déanann", "déanann",
                "déanaimid", "déanann", "déanann",
                "déantar", "déanann"
        );

        testTense(
                obj.getJSONObject("past_independent"),
                "rinne", "rinne", "rinne",
                "rinneamar", "rinne", "rinne",
                "rinneadh", "rinne"
        );

        testTense(
                obj.getJSONObject("past_dependent"),
                "dhearna", "dhearna", "dhearna",
                "dhearnamar", "dhearna", "dhearna",
                "dhearnadh", "ndearna"
        );

        testTense(
                obj.getJSONObject("imperfect"),
                "dhéanainn", "dhéantá", "dhéanadh",
                "dhéanaimis",  "dhéanadh", "dhéanaidis",
                "dhéantaí", "dhéanadh"
        );

        testTense(
                obj.getJSONObject("future"),
                "déanfaidh", "déanfaidh", "déanfaidh",
                "déanfaimid", "déanfaidh", "déanfaidh",
                "déanfar", "déanfaidh"
        );

        testTense(
                obj.getJSONObject("conditional"),
                "dhéanfainn", "dhéanfá", "dhéanfadh",
                "dhéanfaimis", "dhéanfadh", "dhéanfaidís",
                "dhéanfaí", "dhéanfadh"
        );

        testImperative(
                obj.getJSONObject("imperative"),
                "déan", "déanaigí"
        );
    }

    @Test
    public void testAisghair(){
        JSONObject obj = getVerb(new IrishDictionary(), "aisghair");
        testTense(
                obj.getJSONObject("present"),
                "aisghairim", "aisghaireann", "aisghaireann",
                "aisghairimid", "aisghaireann", "aisghaireann",
                "aisghairtear", "aisghaireann"
        );
        testTense(
                obj.getJSONObject("preterite"),
                "aisghair", "aisghair", "aisghair",
                "aisghaireamar", "aisghair", "aisghair",
                "aisghaireadh", "aisghair"
        );
        testTense(
                obj.getJSONObject("imperfect"),
                "aisghairinn", "aisghairteá", "aisghaireadh",
                "aisghairimis", "aisghaireadh", "aisghairidis",
                "aisghairtí", "aisghaireadh"
        );
    }

    public static void testImperative(
            JSONObject tense,
            String singSecond,
            String plurSecond
    ){
        testTense(
                tense,
                new String[]{},
                new String[]{singSecond},
                new String[]{},
                new String[]{},
                new String[]{plurSecond},
                new String[]{},
                new String[]{},
                new String[]{}
        );
    }

    public static void testTense(
            JSONObject tense,
            String singFirst,
            String singSecond,
            String singThird,
            String plurFirst,
            String plurSecond,
            String plurThird,
            String impersonal,
            String analytic
    ){
        testTense(
                tense,
                new String[]{singFirst},
                new String[]{singSecond},
                new String[]{singThird},
                new String[]{plurFirst},
                new String[]{plurSecond},
                new String[]{plurThird},
                new String[]{impersonal},
                new String[]{analytic}
        );
    }
}
