package cymru.prv.api.geiriadur.welsh;

import cymru.prv.api.geiriadur.common.AbstractTestClass;
import cymru.prv.api.geiriadur.linguistics.common.WordType;
import cymru.prv.api.geiriadur.linguistics.welsh.WelshDictionary;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class WelshAdjectiveTests extends AbstractTestClass {

    public static void testAdjective(
            JSONObject obj,
            String normalForm,
            String equative,
            String comparative,
            String superlative
    ){
        Assert.assertEquals(normalForm, obj.getString("normalForm"));
        Assert.assertArrayEquals(new String[]{equative}, jsonArrayToStringArray(obj.getJSONArray("equative")));
        Assert.assertArrayEquals(new String[]{comparative}, jsonArrayToStringArray(obj.getJSONArray("comparative")));
        Assert.assertArrayEquals(new String[]{superlative}, jsonArrayToStringArray(obj.getJSONArray("superlative")));
    }



    @Test
    public void testDa(){
        JSONObject obj = new WelshDictionary().getWord("da", WordType.adjective).getJSONObject(0);
        testAdjective(obj, "da", "cystal","gwell", "gorau");

        MutationTests.assertMutations(
                obj.getJSONObject("mutations"),
                "da", "dda", "na", ""
        );
    }

    @Test
    public void testOfnadwy(){
        JSONObject obj = new WelshDictionary().getWord("ofnadwy", WordType.adjective).getJSONObject(0);
        testAdjective(obj, "ofnadwy", "mor ofnadwy","mwy ofnadwy", "mwya ofnadwy");
    }

    @Test
    public void testGwlyb(){
        JSONObject obj = new WelshDictionary().getWord("gwlyb", WordType.adjective).getJSONObject(0);
        testAdjective(obj, "gwlyb", "mor wlyb","gwlypach", "gwlypaf");
    }
}
