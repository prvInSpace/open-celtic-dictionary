package cymru.prv.api.geiriadur.welsh;

import cymru.prv.api.geiriadur.common.AbstractTestClass;
import cymru.prv.api.geiriadur.linguistics.common.WordType;
import cymru.prv.api.geiriadur.linguistics.welsh.WelshDictionary;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class WelshVerbTests extends AbstractTestClass {

    private void TestTense(
            JSONObject tense,
            String singFirst,
            String singSecond,
            String singThird,
            String plurFirst,
            String plurSecond,
            String plurThird
    ){
        TestTense(tense,
                new String[]{singFirst},
                new String[]{singSecond},
                new String[]{singThird},
                new String[]{plurFirst},
                new String[]{plurSecond},
                new String[]{plurThird});
    }

    private void TestTense(
            JSONObject tense,
            String[] singFirst,
            String[] singSecond,
            String[] singThird,
            String[] plurFirst,
            String[] plurSecond,
            String[] plurThird
    ){
        Assert.assertArrayEquals(singFirst, jsonArrayToStringArray(tense.getJSONArray("singFirst")));
        Assert.assertArrayEquals(singSecond, jsonArrayToStringArray(tense.getJSONArray("singSecond")));
        Assert.assertArrayEquals(singThird, jsonArrayToStringArray(tense.getJSONArray("singThird")));

        Assert.assertArrayEquals(plurFirst, jsonArrayToStringArray(tense.getJSONArray("plurFirst")));
        Assert.assertArrayEquals(plurSecond, jsonArrayToStringArray(tense.getJSONArray("plurSecond")));
        Assert.assertArrayEquals(plurThird, jsonArrayToStringArray(tense.getJSONArray("plurThird")));

    }



    @Test
    public void BodTests(){
        JSONObject obj = getVerb(new WelshDictionary(), "bod");
        Assert.assertEquals(obj.getString("normalForm"), "bod");

        TestTense(
                obj.getJSONObject("preterite"),
                "bues", "buest", "buodd",
                "buon", "buoch", "buon"
        );

        TestTense(
                obj.getJSONObject("future"),
                "byddaf", "byddi", "bydd",
                "byddwn", "byddwch", "byddan"
        );

        TestTense(
                obj.getJSONObject("imperative"),
                new String[]{}, new String[]{"bydda"}, new String[]{},
                new String[]{}, new String[]{"byddwch"}, new String[]{}
        );

        TestTense(
                obj.getJSONObject("conditional"),
                "byddwn", "byddet", "byddai",
                "bydden", "byddech", "bydden"
        );

        MutationTests.assertMutations(
                obj.getJSONObject("mutations"),
                "bod", "fod", "mod", ""
        );
    }

    @Test
    public void TestDechrau(){
        JSONObject obj = new WelshDictionary().getWord("dechrau", WordType.verb).getJSONObject(0);;

        TestTense(
                obj.getJSONObject("preterite"),
                "dechreuais", "dechreuaist", "dechreuodd",
                "dechreuon", "dechreuoch", "dechreuon"
        );

        TestTense(
                obj.getJSONObject("future"),
                new String[]{"dechreuaf"}, new String[]{"dechreui"}, new String[]{"dechreuiff", "dechreuith"},
                new String[]{"dechreuwn"}, new String[]{"dechreuwch"}, new String[]{"dechreuan"}
        );

        TestTense(
                obj.getJSONObject("conditional"),
                "dechreuwn", "dechreuet", "dechreuai",
                "dechreuen", "dechreuech", "dechreuen"
        );

        TestTense(
                obj.getJSONObject("imperative"),
                new String[]{}, new String[]{"dechreua"}, new String[]{},
                new String[]{}, new String[]{"dechreuwch"}, new String[]{}
        );
    }

    @Test
    public void TestMynd(){
        JSONObject obj = new WelshDictionary().getWord("mynd", WordType.verb).getJSONObject(0);

        TestTense(
                obj.getJSONObject("preterite"),
                "es", "est", "aeth",
                "aethon", "aethoch", "aethon"
        );

        TestTense(
                obj.getJSONObject("future"),
                new String[]{"af"}, new String[]{"ei"}, new String[]{"aiff", "eith"},
                new String[]{"awn"}, new String[]{"ewch"}, new String[]{"ân"}
        );

        TestTense(
                obj.getJSONObject("conditional"),
                "awn", "aet", "âi",
                "aen", "aech", "aen"
        );

        TestTense(
                obj.getJSONObject("imperative"),
                new String[]{}, new String[]{"cer"}, new String[]{},
                new String[]{}, new String[]{"cerwch"}, new String[]{}
        );
    }

    @Test
    public void testDylu(){
        JSONObject obj = new WelshDictionary().getWord("dylu").getJSONObject(0);
        Assert.assertFalse(obj.has("past"));
        Assert.assertFalse(obj.has("future"));
        Assert.assertFalse(obj.has("present"));
        Assert.assertFalse(obj.has("imperative"));
        Assert.assertTrue(obj.has("pluperfect"));
        Assert.assertTrue(obj.has("conditional"));
    }

    @Test
    public void testChwarae() {
        JSONObject obj = new WelshDictionary().getWord("chwarae", WordType.verb).getJSONObject(0);

        TestTense(
                obj.getJSONObject("preterite"),
                "chwaraeais", "chwaraeaist", "chwaraeodd",
                "chwaraeon", "chwaraeoch", "chwaraeon"
        );
        TestTense(
                obj.getJSONObject("future"),
                new String[]{"chwaraeaf"}, new String[]{"chwaraei"}, new String[]{"chwaraeiff", "chwaraeith"},
                new String[]{"chwaraewn"}, new String[]{"chwaraewch"}, new String[]{"chwaraean"}
        );
        TestTense(
                obj.getJSONObject("conditional"),
                "chwaraewn", "chwaraeet", "chwaraeai",
                "chwaraeen", "chwaraeech", "chwaraeen"
        );
        Assert.assertFalse(obj.has("present"));
    }

}
