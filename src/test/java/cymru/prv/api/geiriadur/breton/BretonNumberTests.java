package cymru.prv.api.geiriadur.breton;

import cymru.prv.api.geiriadur.linguistics.breton.BretonFemNumberToText;
import org.junit.Assert;
import org.junit.Test;

public class BretonNumberTests {

    private String getNumber(long number){
        return new BretonFemNumberToText().convertToText(number);
    }

    @Test
    public void Test9(){
        Assert.assertEquals("nav", getNumber(9));
    }

    @Test
    public void Test92(){
        Assert.assertEquals("daouzek ha pevar-ugent", getNumber(92));
    }

    @Test
    public void test178(){
        Assert.assertEquals("kant triwec'h ha tri-ugent", getNumber(178));
    }

    @Test
    public void test356(){
        Assert.assertEquals("tri c'hant c'hwec'h hag hanter-kant", getNumber(356));
    }

    @Test
    public void test139million(){
        Assert.assertEquals("kant nav biliard ha tregont", getNumber(139000000000000000L));
    }
}
