package cymru.prv.api.geiriadur.common;

import cymru.prv.api.geiriadur.AbstractDictionary;
import cymru.prv.api.geiriadur.linguistics.common.WordType;
import org.json.JSONArray;
import org.json.JSONObject;


public abstract class AbstractTestClass {

    public JSONObject getAdjective(AbstractDictionary dictionary, String name){
        return dictionary.getWord(name, WordType.adjective).getJSONObject(0);
    }

    public JSONObject getNoun(AbstractDictionary dictionary, String name){
        return dictionary.getWord(name, WordType.noun).getJSONObject(0);
    }

    public JSONObject getVerb(AbstractDictionary dictionary, String name){
        return dictionary.getWord(name, WordType.verb).getJSONObject(0);
    }

    public JSONObject getPrep(AbstractDictionary dictionary, String name){
        return  dictionary.getWord(name, WordType.preposition).getJSONObject(0);
    }

    public static String[] jsonArrayToStringArray(JSONArray array) {
        String[] strings = new String[array.length()];
        for(int i = 0; i < array.length(); ++i)
            strings[i] = array.getString(i);
        return strings;
    }

}
